package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.GeodeticHandRow


@Dao
interface GeodeticHandRowDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertJournalRow(journalRow: GeodeticHandRow): Long

	@Query("SELECT * FROM GeodeticHandRow where measurementsId = :id ")
	fun getMeasurementById(id: Long): List<GeodeticHandRow>

	@Update
	fun updateGeodeticHandRows(list: List<GeodeticHandRow>)

	@Delete
	fun deleteGeodeticHandRows(list: List<GeodeticHandRow>)

}