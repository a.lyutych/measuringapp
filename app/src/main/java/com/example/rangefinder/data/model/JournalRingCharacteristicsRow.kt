package com.example.rangefinder.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
	foreignKeys = [ForeignKey(
		entity = CommonMeasurements::class,
		parentColumns = arrayOf("id"),
		childColumns = arrayOf("measurementsId"),
		onDelete = ForeignKey.CASCADE
	)]
)
class JournalRingCharacteristicsRow : Measurement {
	@PrimaryKey(autoGenerate = true)
	var id: Long = 0
	var rowNumber: Int? = 0
	var age: Int? = 0
	var tier: String? = null
	var generation: String? = null
	var breed: String? = null
	var diameter: Double? = null
	var height: Double? = null
	var category: String? = null
	var direction: Double? = null
	var horizontalDistance: Double? = null
	var measurementsId: Long = 0


	override fun toString(): String {
		return "$rowNumber,$tier,$generation,$age"
	}
}