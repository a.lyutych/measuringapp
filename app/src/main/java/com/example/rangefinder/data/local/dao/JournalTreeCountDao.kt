package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.JournalTreeCount

@Dao
interface JournalTreeCountDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertTableTreeCountRow(journalTreeCount: JournalTreeCount): Long

	@Update
	fun updateTableTreeCountRow(journalTreeCount: JournalTreeCount)

	@Delete
	fun deleteTableTreeCountRow(journalTreeCount: JournalTreeCount)

	@Query("SELECT * FROM JournalTreeCount where measurementsId = :id ")
	fun getMeasurementById(id: Long): List<JournalTreeCount>
}