package com.example.rangefinder.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
	foreignKeys = [ForeignKey(
		entity = CommonMeasurements::class,
		parentColumns = arrayOf("id"),
		childColumns = arrayOf("measurementsId"),
		onDelete = ForeignKey.CASCADE
	)]
)
class JournalSquareRow : Measurement {
	@PrimaryKey(autoGenerate = true)
	var id: Long = 0
	var rowNumber: Int? = 0
	var treeNumber: Int? = 0
	var squareNumber: Int? = 0
	var breed: String? = null
	var tier: String? = null
	var generation: String? = null
	var age: Int? = 0
	var diameterOne: Double? = null
	var diameterTwo: Double? = null
	var height: Double? = null
	var heightTwo: Double? = null
	var heightThree: Double? = null
	var diameterCrownOne: Double? = null
	var diameterCrownTwo: Double? = null
	var category: String? = null
	var distanceCrown: Double? = null
	var coordinateX: Double? = null
	var coordinateY: Double? = null
	var crownFormVertical: String? = null
	var crownFormHorizontal: String? = null
	var measurementsId: Long = 0

	override fun toString(): String {
		return "$rowNumber,$treeNumber,$squareNumber,$age"
	}
}