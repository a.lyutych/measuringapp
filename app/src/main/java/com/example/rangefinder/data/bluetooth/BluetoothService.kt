package com.example.rangefinder.data.bluetooth

import android.bluetooth.BluetoothDevice
import android.bluetooth.BluetoothSocket
import android.content.ContentValues.TAG
import android.media.RingtoneManager
import android.net.Uri
import android.util.Log
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import java.io.BufferedReader
import java.io.IOException
import java.io.InputStream
import java.util.*
import kotlin.collections.ArrayList
import kotlinx.android.synthetic.main.activity_measurement_setup.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch

class BluetoothService {

	class ConnectToSocket(bluetoothDevice: BluetoothDevice, isRangeFinder: Boolean) : Thread() {
		private val device = bluetoothDevice
		private var mmSocket: BluetoothSocket? = null
		private val isRange = isRangeFinder
		override fun run() {
			try {
				mmSocket = device.createRfcommSocketToServiceRecord(MY_UUID)
				mmSocket!!.connect()

				if (isRange) {
					CommonInfo.instance.bluetoothConnectedLiveData.postValue(device.name)
					mmSocket?.let { ReadDataForRangeFinder(it).run() }
				} else {
					CommonInfo.instance.bluetoothConnectedLiveData.postValue(device.name)

					mmSocket?.let { ReadData(it).run() }
				}
				return
			} catch (e: Exception) {
				e.toString()
				CommonInfo.instance.bluetoothConnectedLiveData.postValue("Не удалось соединиться " +
						"с ${device.name}")
			}
			try {
				mmSocket = device.javaClass.getMethod(
					"createRfcommSocket",
					*arrayOf<Class<*>>(Int::class.javaPrimitiveType!!)
				).invoke(device, 1) as BluetoothSocket
				mmSocket!!.connect()

				CommonInfo.instance.bluetoothConnectedLiveData.postValue(device.name)
				return
			} catch (e: IOException) {
				CommonInfo.instance.bluetoothConnectedLiveData.postValue("Не удалось соединиться " +
						"c $device.name")
			}

		}
	}

	private class ReadData(
		private val mmSocket: BluetoothSocket
	) : Thread() {
		private val mmInStream: InputStream = mmSocket.inputStream
		var lineList = ArrayList<String>()
		override fun run() {
			val reader = BufferedReader(mmInStream.reader())
			while (true) {
				try {
					lineList.add(reader.readLine())
				} catch (e: Exception) {
					CommonInfo.instance.bluetoothConnectedLiveData.postValue("Соединение прервано" +
							" с ${mmSocket.remoteDevice.name}")
					break
				}
				if (((lineList.size == 3 && lineList[3]?.contains("LEN")) || lineList.size == 2)
					&& lineList[1]?.contains("PHGF")
				) {
					GlobalScope.launch(context = Dispatchers.Main) {
						CommonInfo.instance.journalTreeCountRowLiveData.postValue(lineList)
						delay(10)
						lineList.clear()
					}
				}
			}
		}
		fun cancel() {
			try {
				mmSocket.close()
			} catch (e: IOException) {
				Log.e(TAG, "Could not close the connect socket", e)
			}
		}
	}

	private class ReadDataForRangeFinder(
		private val mmSocket: BluetoothSocket
	) : Thread() {

		private val mmBuffer: ByteArray = ByteArray(1024)
		lateinit var line: String


		override fun run() {

				while (true) {
					try {
						val mmInStream: InputStream = mmSocket.inputStream
						val reader = BufferedReader(mmInStream.reader())
						line = reader.readLine()
						/*mmInStream.read(mmBuffer)*/

					} catch (e: Exception) {
						CommonInfo.instance.bluetoothConnectedLiveData.postValue("Соединение прервано" +
								" с ${mmSocket.remoteDevice.name}")
						break
					}

/*                val result = mmBuffer.decodeToString()*/
					if (line.contains("PLTIT")) {
						CommonInfo.instance.journalRowLiveData.postValue(line)
						line = ""
					}
				}
		}

		fun cancel() {
			try {
				mmSocket.close()
			} catch (e: IOException) {
				Log.e(TAG, "Could not close the connect socket", e)
			}
		}
	}


	companion object {
		private val MY_UUID = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB")
	}
}

