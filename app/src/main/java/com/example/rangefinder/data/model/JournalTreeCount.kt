package com.example.rangefinder.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
	foreignKeys = [ForeignKey(
		entity = CommonMeasurements::class,
		parentColumns = arrayOf("id"),
		childColumns = arrayOf("measurementsId"),
		onDelete = ForeignKey.CASCADE
	)]
)
class JournalTreeCount : Measurement {
	@PrimaryKey(autoGenerate = true)
	var id: Long = 0
	var rowNumber: Int? = 0
	var tier: String? = null
	var generation: String? = null
	var breed: String? = null
	var diameter: Double? = null
	var category: String? = null
	var measurementsId: Long = 0

	override fun toString(): String {
		return "$rowNumber,$breed,$diameter,$category,$tier,$generation"
	}
}