package com.example.rangefinder.data.dto

class GeodeticDto {
	var travelDirection: String? = null
	var magneticDeclination: Double? = null
	var firstPointCoordinateX: Double? = null
	var firstPointCoordinateY: Double? = null
	var square: Double? = null
	var rows: List<GeodeticRowDto>? = null
}