package com.example.rangefinder.data.bluetooth.transfromer

import com.example.rangefinder.data.model.JournalTreeCount

class JournalTreeCountRowTransformer {

	fun transform(row: ArrayList<String>): JournalTreeCount {
		val firstLine = row.getOrNull(0)?.split(",")?.toList()
		val secondLine = row.getOrNull(1)?.split(",")?.toList()
		return JournalTreeCount().apply {
			breed = firstLine?.getOrNull(BREED_POSITION)
			diameter = secondLine?.getOrNull(DIAMETER_POSITION)?.toDoubleOrNull()?.div(10)
		}
	}

	companion object {

		const val BREED_POSITION = 3
		const val DIAMETER_POSITION = 3
	}
}