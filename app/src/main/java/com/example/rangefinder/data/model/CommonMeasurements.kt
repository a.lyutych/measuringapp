package com.example.rangefinder.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.example.rangefinder.ui.measurement.TravelDirection
import java.io.Serializable
import java.util.*

@Entity
data class CommonMeasurements(

	@PrimaryKey(autoGenerate = true)
	var id: Long = 0,
	var name: String = "",
	var magneticDeclination: Double? = null,
	var firstPointCoordinateX: Double? = null,
	var firstPointCoordinateY: Double? = null,
	var isEditMode: Boolean? = null,
	var created: Date? = null,
	var isOneMeasurementHeight: Boolean = true,
	var numberOfMeasurementType: Int? = null,
	var initialHeight: Double? = null,
	var travelDirection: String = TravelDirection.RIGHT.label,
	var handModeFirstAngle: String = "",
	var handModeAngle: String = ""

) : Serializable
