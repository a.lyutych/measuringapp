package com.example.rangefinder.data.model

import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey

@Entity(
    foreignKeys = [ForeignKey(
        entity = CommonMeasurements::class,
        parentColumns = arrayOf("id"),
        childColumns = arrayOf("measurementsId"),
        onDelete = ForeignKey.CASCADE
    )]
)
 open class GeodeticRow : Measurement {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
    var rowNumber: Int? = null
    var bindingLine: Boolean = false
    var horizontalDistance: Double? = null
    var azimuth: Double? = null
    var directionalAngle: Double? = null
    var coordinateX: Double? = null
    var coordinateY: Double? = null
    var measurementsId: Long = 0
    var rhumb: String? = null
    var insideCorner: Double? = null
    var verticalAngle: Double? = null
    var slantDistance: Double? = null
}