package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.CommonMeasurements

@Dao
interface CommonMeasurementsDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertMeasurements(commonMeasurements: CommonMeasurements) : Long

    @Update
    fun updateMeasurements(commonMeasurements: CommonMeasurements)

    @Delete
    fun deleteMeasurements(commonMeasurements: CommonMeasurements)

    @Query("SELECT * FROM CommonMeasurements")
    fun getMeasurements(): List<CommonMeasurements>

    @Query("SELECT * FROM CommonMeasurements where id = :id ")
    fun getMeasuremenstById(id: Long): CommonMeasurements

    @Query("SELECT * FROM CommonMeasurements where name = :name ")
    fun getMeasurementsByName(name: String): CommonMeasurements?
}