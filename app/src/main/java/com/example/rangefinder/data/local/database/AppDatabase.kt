package com.example.rangefinder.data.local.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.example.rangefinder.data.local.dao.*
import com.example.rangefinder.data.model.*

@Database(
    entities = [
        CommonMeasurements::class,
        GeodeticRow::class,
        JournalTreeCount::class,
        JournalRingCharacteristicsRow::class,
        JournalSquareRow::class,
        JournalHeightRow::class,
        GeodeticHandRow::class
    ], version = 9
)
@TypeConverters(DateTypeConverter::class)
abstract class AppDatabase : RoomDatabase(){

    abstract fun commonMeasurementsDao(): CommonMeasurementsDao
    abstract fun journalRowDao(): JournalRowDao
    abstract fun journalCircleDao(): JournalCircleDao
    abstract fun journalSquareDao(): JournalSquareDao
    abstract fun tableTreeCountDao(): JournalTreeCountDao
    abstract fun journalHeightRowDao(): JournalHeightRowDao
    abstract fun geodeticHandRowDao(): GeodeticHandRowDao

    companion object {
        var INSTANCE: AppDatabase? = null

        fun getAppDataBase(context: Context): AppDatabase? {
            if (INSTANCE == null) {
                synchronized(AppDatabase::class) {
                    INSTANCE = Room.databaseBuilder(
                        context.applicationContext,
                        AppDatabase::class.java,
                        "myDB"
                    ).fallbackToDestructiveMigration().build()
                }
            }
            return INSTANCE
        }

        fun destroyDataBase() {
            INSTANCE = null
        }
    }
}