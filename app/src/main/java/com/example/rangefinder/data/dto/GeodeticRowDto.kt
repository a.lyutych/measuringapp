package com.example.rangefinder.data.dto

class GeodeticRowDto {
	var rowNumber: Int? = null
	var azimuth: Double? = null
	var verticalAngle: Double? = null
	var slantDistance: Double? = null
	var coordinateX: Double? = null
	var coordinateY: Double? = null
	var directionAngle: Double? = null
	var rhumb: String? = null
	var isBindingLine: Boolean? = null
	var insideAngle: Double? = null
	var horizontalDistance: Double? = null
}