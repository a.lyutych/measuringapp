package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.GeodeticRow

@Dao
interface JournalRowDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertJournalRow(geodeticRow: GeodeticRow) :Long

    @Query("SELECT * FROM GeodeticRow where measurementsId = :id ")
    fun getMeasurementById(id: Long): List<GeodeticRow>

    @Delete
    fun deleteGeodeticRows(list: List<GeodeticRow>)

    @Update
    fun updateGeodeticRows(list: List<GeodeticRow>)
}