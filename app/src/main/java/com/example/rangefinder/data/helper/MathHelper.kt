package com.example.rangefinder.data.helper

import com.example.rangefinder.removeExtraDegrees
import com.example.rangefinder.roundTo1DecimalPlaces
import com.example.rangefinder.ui.measurement.TravelDirection

class MathHelper {
	companion object {
		fun fromAzimuthToRhumb(azimuth: Double?): String? {
			if (azimuth == null) {
				return null
			}
			return when (azimuth.removeExtraDegrees()) {
				in 0.0..90.0 -> "${azimuth.roundTo1DecimalPlaces()} СВ"
				in 90.0..180.0 -> "${(180 - azimuth).roundTo1DecimalPlaces()} ЮВ"
				in 180.0..270.0 -> "${(azimuth - 180).roundTo1DecimalPlaces()} ЮЗ"
				in 270.0..360.0 -> "${(360 - azimuth).roundTo1DecimalPlaces()} СЗ"
				else -> null
			}
		}

		fun fromDirectionalToInside(
			direction: String,
			directionalAngle: Double?,
			previousDirectionAngle: Double?
		): Double? {
			if (directionalAngle == null || previousDirectionAngle == null) {
				return null
			}
			return when (direction) {
				TravelDirection.RIGHT.label -> previousDirectionAngle.plus(180.0)
					.minus(directionalAngle)
				TravelDirection.LEFT.label -> 180.0.plus(directionalAngle)
					.minus(previousDirectionAngle)
				else -> null
			}?.removeExtraDegrees()?.roundTo1DecimalPlaces()
		}

		fun fromRhumbToAzimuth(rhumb: String?): Double? {
			if (rhumb == null) {
				return null
			}
			return when {
				rhumb.contains("СВ") -> rhumb.replace("СВ", "").toDoubleOrNull()
				rhumb.contains("ЮВ") -> rhumb.replace("ЮВ", "").toDoubleOrNull()?.let {
					180.0.minus(it)
				}
				rhumb.contains("ЮЗ") -> rhumb.replace("ЮЗ", "").toDoubleOrNull()?.plus(180.0)
				rhumb.contains("СЗ") -> rhumb.replace("СЗ", "").toDoubleOrNull()?.let {
					360.0.minus(it)
				}
				else -> null
			}?.removeExtraDegrees()?.roundTo1DecimalPlaces()
		}

		fun getDirectionalFromInside(
			previousDirectionAngle: Double?,
			insideAngle: Double?,
			direction: String
		): Double? {
			if (insideAngle == null || previousDirectionAngle == null) {
				return null
			}
			return when (direction) {
				TravelDirection.RIGHT.label -> previousDirectionAngle.plus(180.0).minus(insideAngle)
				TravelDirection.LEFT.label -> previousDirectionAngle.minus(180.0).plus(insideAngle)
				else -> null
			}?.removeExtraDegrees()?.roundTo1DecimalPlaces()
		}
	}
}