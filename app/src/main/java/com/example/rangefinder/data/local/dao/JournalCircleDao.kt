package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.JournalRingCharacteristicsRow

@Dao
interface JournalCircleDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertCircleRow( journalRingCharacteristicsRow : JournalRingCharacteristicsRow):Long
	@Update
	fun updateCircleRow(journalRingCharacteristicsRow : JournalRingCharacteristicsRow)

	@Delete
	fun deleteCircleRow(journalRingCharacteristicsRow : JournalRingCharacteristicsRow)

	@Query("SELECT * FROM JournalRingCharacteristicsRow where measurementsId = :id ")
	fun getMeasurementById(id: Long): List<JournalRingCharacteristicsRow>
}