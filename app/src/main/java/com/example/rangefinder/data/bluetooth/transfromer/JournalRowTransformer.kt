package com.example.rangefinder.data.bluetooth.transfromer

import com.example.rangefinder.data.model.GeodeticRow

class JournalRowTransformer {

    fun transform(row: String): GeodeticRow {
        val list = row.split(",").toList()
        return GeodeticRow().apply {
            azimuth = list.getOrNull(AZIMUTH_POSITION)?.toDoubleOrNull()
            horizontalDistance = list.getOrNull(HORIZONTAL_DISTANCE_POSITION)?.toDoubleOrNull()
            slantDistance = list.getOrNull(SLANT_DISTANCE_POSITION)?.toDoubleOrNull()
            verticalAngle = list.getOrNull(VERTICAL_ANGLE_POSITION)?.toDoubleOrNull()
        }
    }

    companion object {
        const val HORIZONTAL_DISTANCE_POSITION = 2
        const val AZIMUTH_POSITION = 4
        const val VERTICAL_ANGLE_POSITION = 6
        const val SLANT_DISTANCE_POSITION = 8
    }
}