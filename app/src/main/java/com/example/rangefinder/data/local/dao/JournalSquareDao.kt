package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.JournalSquareRow

@Dao
interface JournalSquareDao {
	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertSquareRow( journalSquareRow : JournalSquareRow):Long
	@Update
	fun updateSquareRow(journalSquareRow : JournalSquareRow)

	@Delete
	fun deleteSquareRow(journalSquareRow : JournalSquareRow)

	@Query("SELECT * FROM JournalSquareRow where measurementsId = :id ")
	fun getMeasurementById(id: Long): List<JournalSquareRow>
}