package com.example.rangefinder.data.local.dao

import androidx.room.*
import com.example.rangefinder.data.model.JournalHeightRow

@Dao
interface JournalHeightRowDao {

	@Insert(onConflict = OnConflictStrategy.REPLACE)
	fun insertTableTreeCountRow(journalTreeCount: JournalHeightRow): Long

	@Update
	fun updateTableTreeCountRow(journalTreeCount: JournalHeightRow)

	@Delete
	fun deleteTableTreeCountRow(journalTreeCount: JournalHeightRow)

	@Query("SELECT * FROM JournalHeightRow where measurementsId = :id ")
	fun getMeasurementById(id: Long): List<JournalHeightRow>
}