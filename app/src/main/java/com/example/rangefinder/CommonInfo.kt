package com.example.rangefinder

import android.bluetooth.BluetoothDevice
import androidx.lifecycle.MutableLiveData

class CommonInfo private constructor() {

    val journalRowLiveData = MutableLiveData<String>()
    val bluetoothDevicesLiveData = MutableLiveData<BluetoothDevice>()
    val bluetoothConnectedLiveData = MutableLiveData<String>()
    val journalTreeCountRowLiveData = MutableLiveData<ArrayList<String>>()
    private object HOLDER {
        val INSTANCE = CommonInfo()
    }

    companion object {
        val instance: CommonInfo by lazy { HOLDER.INSTANCE }
    }
}