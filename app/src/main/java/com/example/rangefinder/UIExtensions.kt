package com.example.rangefinder

import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import java.math.BigDecimal

fun AppCompatActivity.getFragmentInContainer(containerId: Int): Fragment? =
	supportFragmentManager.findFragmentById(containerId)

fun ViewGroup.disableAllInnerEditTexts(enable: Boolean = false) {
	(0..childCount).forEach {
		val child = this.getChildAt(it)
		if (child is EditText) {
			child.isEnabled = enable
		} else if (child is ViewGroup) {
			child.disableAllInnerEditTexts(enable)
		}
	}
}

fun ViewGroup.disableAllInnerSpinners(enable: Boolean = false) {
	(0..childCount).forEach {
		val child = this.getChildAt(it)
		if (child is Spinner) {
			child.isEnabled = enable
		} else if (child is ViewGroup) {
			child.disableAllInnerSpinners(enable)
		}
	}
}

fun ViewGroup.disableAllInnerCheckBoxes(enable: Boolean = false) {
	(0..childCount).forEach {
		val child = this.getChildAt(it)
		if (child is CheckBox) {
			child.isEnabled = enable
		} else if (child is ViewGroup) {
			child.disableAllInnerCheckBoxes()
		}
	}
}

fun Spinner.onSelected(callback: (position: Int) -> Unit) {
	this.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
		override fun onNothingSelected(parent: AdapterView<*>?) = Unit

		override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
			callback.invoke(position)
		}

	}
}
fun Double.removeExtraDegrees(): Double {
	if (this > 360) {
		return this.minus(360)
	}
	return this
}

fun Double.roundTo1DecimalPlaces() =
	BigDecimal(this).setScale(1, BigDecimal.ROUND_HALF_UP).toDouble()

fun Double.roundTo5DecimalPlaces() =
	BigDecimal(this).setScale(5, BigDecimal.ROUND_HALF_UP).toDouble()

fun Double.roundTo7DecimalPlaces() =
	BigDecimal(this).setScale(7, BigDecimal.ROUND_HALF_UP).toDouble()