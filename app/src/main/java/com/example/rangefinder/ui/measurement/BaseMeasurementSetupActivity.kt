package com.example.rangefinder.ui.measurement

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.BluetoothService
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.onSelected
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.main.isLight
import com.example.rangefinder.ui.measurement.circle.setup.CircleSetupActivity
import com.example.rangefinder.ui.measurement.geodetic.setup.GeodeticSetupActivity
import com.example.rangefinder.ui.measurement.height.setup.HeightSetupActivity
import com.example.rangefinder.ui.measurement.squre.setup.SquareSetupActivity
import com.example.rangefinder.ui.measurement.treecount.setup.TreeCountSetupActivity
import kotlinx.android.synthetic.main.activity_measurement_setup.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.util.*

abstract class BaseMeasurementSetupActivity : AppCompatActivity() {

	private val bluetoothAdapter: BluetoothAdapter = BluetoothAdapter.getDefaultAdapter()

	private var bluetoothDevices = mutableListOf<BluetoothDevice>()

	private var existingMeasurements: CommonMeasurements? = null

	private val deviceReceiver: DeviceReceiver by lazy { DeviceReceiver() }

	protected open val showSecondSpinner = false

	private var geodeticMeasurementsName: String? = null

	private var measurement_PLOT_ID: Long? = null

	protected open val showFirstSpinner = true

	abstract val measurementType: MeasurementType

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS_NAME = "EXTRA_GEODETIC_MEASUREMENTS_NAME"
		const val EXTRA_EXISTING_MEASUREMENTS = "EXTRA_EXISTING_MEASUREMENTS"
		const val BLUETOOTH_CODE = 9


		fun getLaunchIntent(
			context: Context,
			measurementType: MeasurementType,
			geodeticMeasurementsName: String
		): Intent =
			when (measurementType) {
				MeasurementType.VALUE_OF_DIAMETER ->
					Intent(
						context,
						TreeCountSetupActivity::class.java
					)
				MeasurementType.VALUE_OF_GEODETIC ->
					Intent(
						context,
						GeodeticSetupActivity::class.java
					)
				MeasurementType.CIRCLE ->
					Intent(
						context,
						CircleSetupActivity::class.java
					)
				MeasurementType.SQUARE ->
					Intent(
						context,
						SquareSetupActivity::class.java
					)
				MeasurementType.HEIGHT ->
					Intent(
						context,
						HeightSetupActivity::class.java
					)
			}.apply {
				putExtra(EXTRA_GEODETIC_MEASUREMENTS_NAME, geodeticMeasurementsName)
			}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		if (isLight)setTheme(R.style.AppThemeLight)
		else setTheme(R.style.AppTheme)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_measurement_setup)
		measurement_PLOT_ID = intent.getLongExtra("PLOT_ID", -1)
		geodeticMeasurementsName = if (measurement_PLOT_ID != -1L) {
			measurementType.toString().plus(measurement_PLOT_ID)
		} else {
			intent.getStringExtra(EXTRA_GEODETIC_MEASUREMENTS_NAME)
		}

		if (geodeticMeasurementsName != null) {
			GlobalScope.launch {
				val db = AppDatabase.getAppDataBase(context = applicationContext)
				val geodeticMeasurementsDao = db?.commonMeasurementsDao()

				existingMeasurements =
					geodeticMeasurementsDao?.getMeasurementsByName(geodeticMeasurementsName!!)
				setupContentForCurrentType()
			}
		}
		else{
			setupContentForCurrentType()
		}

		manualInputButton?.setOnClickListener {
			onManualInputClicked()
		}
		
		nextButton.isEnabled = false

		nextButton?.setOnClickListener {
			onNextClicked()
		}

		if (!bluetoothAdapter.isEnabled) {
			val intent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
			startActivityForResult(intent, 9)
		} else {
			initDeviceSpinners()
		}

	}

	override fun onDestroy() {
		super.onDestroy()
		try {
			unregisterReceiver(deviceReceiver)
		} catch (e: IllegalArgumentException){
		}
	}

	 fun initDeviceSpinners() {
		if (!showSecondSpinner) {
			chooseSecondDeviceContainer?.visibility = View.GONE
			status2?.visibility = View.GONE
		}
		if (!showFirstSpinner) {
			chooseDeviceContainer?.visibility = View.GONE
			status1?.visibility = View.GONE
		}
		bluetoothDevices.addAll(bluetoothAdapter.bondedDevices)
		val listOfAdapters = listOf(
			initDeviceSpinner(deviceSpinner, true),
			initDeviceSpinner(secondDeviceSpinner, false)
		)
		CommonInfo.instance.bluetoothDevicesLiveData.observe(this, Observer {
			if (!bluetoothDevices.contains(it)
				&& !it.name.isNullOrEmpty()
				&& it.name.isNotBlank()
			) {
				bluetoothDevices.add(it)
				listOfAdapters.forEach { adapter ->
					adapter.add(it.name)
					adapter.notifyDataSetChanged()

				}

			}
		})
		 listenForBluetoothDevices()
	}

	private fun listenForBluetoothDevices() {
		if (bluetoothAdapter.isDiscovering) {
			bluetoothAdapter.cancelDiscovery()
		}

		bluetoothAdapter.startDiscovery()
		val filter = IntentFilter(BluetoothDevice.ACTION_FOUND)
		registerReceiver(deviceReceiver, filter)
	}

	private fun initDeviceSpinner(spinner: Spinner, isRangeFinder: Boolean) :
			ArrayAdapter<String> {
		val boundDevicesNames = mutableListOf(getString(R.string.undefined_device))
		boundDevicesNames.addAll(bluetoothAdapter.bondedDevices.mapNotNull { it?.name }.toList())
		val adapter = ArrayAdapter(
			this,
			android.R.layout.simple_spinner_item,
			boundDevicesNames
		)
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
		spinner.adapter = adapter
		spinner.onSelected { position ->
			val item = spinner.getItemAtPosition(position)
			if (item != null) {
				val deviceToConnect = bluetoothDevices.firstOrNull { it.name == item as String }
				if (deviceToConnect != null && deviceToConnect.name.isNotEmpty()) {
					GlobalScope.launch {
						BluetoothService.ConnectToSocket(deviceToConnect, isRangeFinder).run()
					}
				}
			}

		}
		CommonInfo.instance.bluetoothConnectedLiveData.observe(this, Observer {
			val device = deviceSpinner.selectedItem.toString()
			val secondDevice = secondDeviceSpinner.selectedItem.toString()
			when {
				device == it -> {
					status_string.setTextColor(Color.parseColor("#00FF00"))
					status_string.setText("Подключен к $device")
					deviceSpinner.setEnabled(false)
				}
				secondDevice == it -> {
					status_string2.setTextColor(Color.parseColor("#00FF00"))
					status_string2.setText("Подключен к $secondDevice")
					secondDeviceSpinner.setEnabled(false)
				}
				it.contains(device) -> {
					status_string.setTextColor(Color.parseColor("#FF0000"))
					status_string.text = it
					deviceSpinner.isEnabled = true
				}
				it.contains(secondDevice) -> {
					status_string2.setTextColor(Color.parseColor("#FF0000"))
					status_string2.text = it
					secondDeviceSpinner.isEnabled = true
				}
			}
			if (!deviceSpinner.isEnabled || !secondDeviceSpinner.isEnabled) {
				nextButton.isEnabled = true
			}

		})
		return adapter
	}

	protected fun saveEmptyGeodeticMeasurements(onSaved: (CommonMeasurements) -> Unit) {
		val db = AppDatabase.getAppDataBase(context = applicationContext)
		val geodeticMeasurementsDao = db?.commonMeasurementsDao()
		var geodeticMeasurements = CommonMeasurements().apply {
			isEditMode = true
			created = Date()
		}
		geodeticMeasurements = insertMeasurementsFields(geodeticMeasurements)
		GlobalScope.launch {
			if(existingMeasurements == null) {
				geodeticMeasurements.name = geodeticMeasurementsName ?: ""
				geodeticMeasurements.numberOfMeasurementType = measurementType.position
				val insertedId = geodeticMeasurementsDao?.insertMeasurements(geodeticMeasurements)
				geodeticMeasurements.id = insertedId!!
			}
			else{
				geodeticMeasurements = existingMeasurements as CommonMeasurements
			}
			onSaved(geodeticMeasurements)
		}
	}

	protected open fun insertMeasurementsFields(measurement: CommonMeasurements): CommonMeasurements {
		return measurement
	}

	abstract fun saveGeodeticMeasurements(callback: (CommonMeasurements) -> Unit)

	abstract fun launchMeasurementScreen(
		commonMeasurements: CommonMeasurements,
		inputMethod: InputMethod
	)

	open fun isValidInput(): Boolean = true

	private fun saveMeasurementAndGoNext(inputMethod: InputMethod) {
		if (isValidInput()) {
			saveGeodeticMeasurements { launchMeasurementScreen(it, inputMethod) }
		} else {
			Toast.makeText(this, getString(R.string.invalid_arguments), Toast.LENGTH_SHORT).show()
		}
	}

	private fun onNextClicked() = saveMeasurementAndGoNext(InputMethod.BLUETOOTH)

	private fun onManualInputClicked() = saveMeasurementAndGoNext(InputMethod.BY_HAND)

	abstract fun getSetupContentFragment(): Fragment?

	private fun setupContentForCurrentType() {
		getSetupContentFragment()?.let {
			supportFragmentManager.beginTransaction().replace(
				R.id.fragmentContainer, it.apply {
					arguments = Bundle().apply {
						putSerializable(EXTRA_EXISTING_MEASUREMENTS, existingMeasurements)
					}
				}
			).commit()
		}
	}

	private inner class DeviceReceiver : BroadcastReceiver() {

		override fun onReceive(context: Context, intent: Intent) {
			when (intent.action) {
				BluetoothDevice.ACTION_FOUND -> {
					val device: BluetoothDevice? =
						intent.getParcelableExtra(BluetoothDevice.EXTRA_DEVICE)
					CommonInfo.instance.bluetoothDevicesLiveData.postValue(device)
				}
			}
		}
	}
}