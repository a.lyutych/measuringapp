package com.example.rangefinder.ui.measurement.treecount

import android.app.Activity
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.core.app.ActivityCompat
import com.example.rangefinder.R
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.getFragmentInContainer
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import java.io.BufferedWriter
import java.io.File
import java.io.FileWriter


class TreeCountActivity : BaseMeasurementActivity() {

	private val requestCodeStoragePermissons: Int = 1000
	private lateinit var pref: SharedPreferences

	val APP_PREFERENCES = "idTreeCount"
	var id = 0

	override fun onCreate(savedInstanceState: Bundle?) {
		pref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE)
		id = pref.getInt(APP_PREFERENCES, 0)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_tree_count)
		attachContentFragment(commonMeasurements)
	}

	private fun attachContentFragment(commonMeasurements: CommonMeasurements?) {
		if (commonMeasurements == null) return

		supportFragmentManager.beginTransaction()
			.replace(R.id.fragmentFrame, TreeCountFragment.newInstance(commonMeasurements))
			.commit()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_measurement_tree_count, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.exitGeodetic -> {
				val builder = AlertDialog.Builder(this)

				with(builder)
				{
					setTitle("Предупреждение")
					setMessage("Вы точно хотите завершить измерения ?")
					setPositiveButton("Да") { dialog, _ ->
						closeApp()
						dialog.dismiss()
					}
					setNegativeButton("Нет") { dialog, _ ->
						dialog.dismiss()
					}

					show()
				}
				return true
			}
			R.id.saveDataToExternalStorage -> {
				saveDataToFile()
				return true
			}
//			R.id.post -> {
//				fakeData()
//				return true
//			}
		}
		return super.onOptionsItemSelected(item)
	}
// fun fakeData(){
//	 val listArray = ArrayList<String>()
//	 listArray.add("PHGF,SPC,1,BZNnn,*5A")
//	 listArray.add("PHGF,DIA,M,98,*19")
//	 CommonInfo.instance.journalTreeCountRowLiveData.postValue(listArray)
// }

	fun saveDataToFile() {
		if (checkSelfPermission(android.Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
			val attachedFragment = getFragmentInContainer(R.id.fragmentFrame)
			val data = (attachedFragment as? TreeCountFragment)?.adapter?.data
			if (data != null) writeToFile(data)
		} else {
			ActivityCompat.requestPermissions(
				this,
				listOf(android.Manifest.permission.WRITE_EXTERNAL_STORAGE).toTypedArray(),
				requestCodeStoragePermissons
			)
		}
	}

	private fun writeToFile(str: MutableList<Measurement>?) {
		val myDir = File("/sdcard/Measurements")
		if (!myDir.exists()) {
			myDir.mkdirs()
		}
		val editor = pref.edit()
		val fileName = "Measurement-$id.json"
		id = id + 1
		editor.putInt(APP_PREFERENCES, id)
		editor.apply()
		val file = File(myDir, fileName)
		if (file.exists()) file.delete()
		file.createNewFile()
		try {
			val writer = BufferedWriter(FileWriter(file))
			writer.write("{\n \"data\": [\n")
			if (str != null) {
				for ((index, value) in str.withIndex()) {
					val list = value.toString().split(",")
					writer.write(
						"\t\t\t{\n" +
								"\t\t\t\t\"id\": ${list[0]},\n" +
								"\t\t\t\t\"breed\": \"${list[1]}\",\n" +
								"\t\t\t\t\"diameter\": ${list[2]},\n" +
								"\t\t\t\t\"category\": \"${list[3]}\",\n" +
								"\t\t\t\t\"tier\": ${list[4]},\n" +
								"\t\t\t\t\"generation\": ${list[5]}\n" +
								"\t\t\t}"
					);
					if (index+1 < str.size) {
						writer.write(",\n")
					} else {
						writer.write(" \n")
					}
				}
			}
			writer.write("\t\t]\n}")
			writer.close();
			Toast.makeText(this, "Успешно сохранено! \n " +
					"$file", Toast.LENGTH_SHORT).show()
		} catch (e: Exception) {
			e.printStackTrace()
		}
	}

	private fun closeApp() {
		GlobalScope.launch {
			val dao = AppDatabase.getAppDataBase(context = applicationContext)
				?.tableTreeCountDao()
			val treeList = dao?.getMeasurementById(commonMeasurements?.id ?: -1)
			val gson = Gson()
			val result = Intent()
			result.putExtra("EXTRA_TREECOUNT_RESULT", gson.toJson(treeList))
			setResult(Activity.RESULT_OK, result)
			finish()
		}
	}
}
