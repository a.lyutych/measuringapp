package com.example.rangefinder.ui.measurement.treecount

import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager

import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.transfromer.JournalTreeCountRowTransformer
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.JournalTreeCount
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.*
import kotlinx.android.synthetic.main.fragment_tree_count.*
import kotlinx.coroutines.*

class TreeCountFragment : Fragment(), MeasurementCallback {

	lateinit var adapter: MeasurementAdapter

	private var measurements: CommonMeasurements? = null

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): TreeCountFragment =
			TreeCountFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_tree_count, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		initRecyclerView()
		initPreviousMeasurements()
		listenForUpdates()
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurements = AppDatabase.getAppDataBase(it)
						?.tableTreeCountDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurements?.isNotEmpty()!!) {
							adapter.data.addAll(measurements)
							adapter.data.add(JournalTreeCount())
						} else {
							showEmptyItem()
						}
						adapter.notifyDataSetChanged()
					}
				}
			}
		}
	}

	private fun listenForUpdates() {
		CommonInfo.instance.journalTreeCountRowLiveData.observe(viewLifecycleOwner, Observer {
			val journalTreeCount = JournalTreeCountRowTransformer().transform(it)
			val ee = journalTreeCount.breed
			if (ee != "null") {
				try {
					val notify: Uri =
						RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
					val r = RingtoneManager.getRingtone(context, notify)
					r.play()
				} catch (e: Exception) {
					e.printStackTrace()
				}
				val lastItem = adapter.data.last() as JournalTreeCount
				lastItem.apply {
					rowNumber = adapter.itemCount - 1
					breed = TreeCodes.values().find {it.rawString == journalTreeCount.breed }?.displayName ?: journalTreeCount.breed
					diameter = journalTreeCount.diameter
					category = TreeCategory.CATEGORY_1.displayName
					tier = Tier.TIER_0.displayName
					generation = Tier.TIER_0.displayName
				}
				adapter.data[adapter.data.lastIndex] = lastItem
				adapter.notifyDataSetChanged()
				onSaveClick(adapter.data[adapter.data.lastIndex])


			}
		})
	}

	private fun initRecyclerView() {
		adapter = MeasurementAdapter(this)
		recyclerView.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@TreeCountFragment.adapter
		}
	}
	private fun showEmptyItem() {
		adapter.data.add(JournalTreeCount())
		adapter.notifyDataSetChanged()
	}


	private fun showEmptyItemIfNecessary() {
		if (getInputMethod() == InputMethod.BY_HAND) {
			adapter.data.add(JournalTreeCount())
			adapter.notifyDataSetChanged()
		}
	}

	private fun getInputMethod(): InputMethod? =
		(activity as? BaseMeasurementActivity)?.inputMethod

	override fun onDeleteClick(measurement: Measurement) {
		if (adapter.itemCount == 1) return

		val journalTreeCount = measurement as? JournalTreeCount ?: return
		GlobalScope.launch {
			context?.let {
				AppDatabase.getAppDataBase(it)
					?.tableTreeCountDao()
					?.deleteTableTreeCountRow(journalTreeCount)

				withContext(Dispatchers.Main) {
					adapter.data.remove(journalTreeCount)
					adapter.notifyDataSetChanged()
				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {


		val journalTreeCount = measurement as? JournalTreeCount ?: return
		measurements ?: return
		 val test =TreeCodes.values().find {it.displayName == journalTreeCount.breed  }?.displayName ?: "not valid"
		if (test == journalTreeCount.breed) {
			GlobalScope.launch {
				context?.let {
					val id = AppDatabase.getAppDataBase(it)
						?.tableTreeCountDao()
						?.insertTableTreeCountRow(journalTreeCount.apply {
							measurementsId = measurements?.id ?: -1
						})
					journalTreeCount.id = id ?: -1

					withContext(Dispatchers.Main) {
						journalTreeCount.rowNumber?.let { adapter.data.set(it, journalTreeCount) }
						showEmptyItem()
						adapter.notifyDataSetChanged()
						recyclerView?.scrollToPosition(adapter.itemCount - 1)
					}
				}
			}
		}
		else{
			Toast.makeText(context, "Нет такой пароды! Измените", Toast.LENGTH_SHORT).show()
		}
	}

	override fun onEditClick(measurement: Measurement) {
		val journalTreeCount = measurement as? JournalTreeCount ?: return
		measurements ?: return
		GlobalScope.launch {
			context?.let {
				val id = AppDatabase.getAppDataBase(it)
					?.tableTreeCountDao()
					?.updateTableTreeCountRow(journalTreeCount.apply {
						measurementsId = measurements?.id ?: -1
					})

				withContext(Dispatchers.Main) {
					journalTreeCount.rowNumber?.let { adapter.data.set(it, journalTreeCount) }
					adapter.notifyItemRangeChanged(0, adapter.data.lastIndex - 1)
					recyclerView?.scrollToPosition(adapter.itemCount - 1)
				}
			}
		}
	}
}
