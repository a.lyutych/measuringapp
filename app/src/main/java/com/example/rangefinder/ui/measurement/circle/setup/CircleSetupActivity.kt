package com.example.rangefinder.ui.measurement.circle.setup

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.getFragmentInContainer
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import com.example.rangefinder.ui.measurement.InputMethod
import com.example.rangefinder.ui.measurement.circle.CircleActivity

class CircleSetupActivity : BaseMeasurementSetupActivity() {

	override val measurementType: MeasurementType = MeasurementType.CIRCLE

	override val showSecondSpinner: Boolean = true

	override fun saveGeodeticMeasurements(callback: (CommonMeasurements) -> Unit) {
		saveEmptyGeodeticMeasurements(callback)
	}
	override fun insertMeasurementsFields(measurement: CommonMeasurements): CommonMeasurements{
		return measurement.apply {
			val setupFragment = getFragmentInContainer(R.id.fragmentContainer)
			initialHeight = setupFragment?.view
				?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.initial_height)?.text.toString()
				.toDoubleOrNull()
			magneticDeclination = setupFragment?.view
				?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.declensionEdit)?.text
				.toString()
				.toDoubleOrNull()
		}
	}

	override fun launchMeasurementScreen(
		commonMeasurements: CommonMeasurements,
		inputMethod: InputMethod
	) {
		startActivityForResult(
			BaseMeasurementActivity.getLaunchIntent(
				this, commonMeasurements, CircleActivity::class.java, inputMethod
			), 1
		)
	}

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (requestCode != BLUETOOTH_CODE) {
			setResult(Activity.RESULT_OK, data)
			finish()
			super.onActivityResult(requestCode, resultCode, data)
		} else {
			initDeviceSpinners()
		}
	}

	override fun isValidInput(): Boolean {
		val setupFragment = getFragmentInContainer(R.id.fragmentContainer)
		var isValid = true
		when {
			setupFragment?.view?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.declensionEdit)?.text.isNullOrBlank() -> {
				isValid = false
			}
			setupFragment?.view?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id
				.initial_height)?.text.isNullOrBlank() -> {
				isValid = false
			}
		}
		return isValid
	}

	override fun getSetupContentFragment(): Fragment? = CircleSetupFragment()
}