package com.example.rangefinder.ui.measurement.squre

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import com.example.rangefinder.R
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch


class SquareActivity: BaseMeasurementActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_squre)
		attachContentFragment(commonMeasurements)
	}
	private fun attachContentFragment(commonMeasurements: CommonMeasurements?) {
		if (commonMeasurements == null) return

		supportFragmentManager.beginTransaction()
			.replace(R.id.fragmentFrame, SquareFragment.newInstance(commonMeasurements))
			.commit()
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_exit, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.exitGeodetic -> {
				val builder = AlertDialog.Builder(this)

				with(builder)
				{
					setTitle("Предупреждение")
					setMessage("Вы точно хотите завершить измерения ?")
					setPositiveButton("Да") { dialog, _ ->
						closeApp()
						dialog.dismiss()
					}
					setNegativeButton("Нет") { dialog, _ ->
						dialog.dismiss()
					}

					show()
				}
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	private fun closeApp() {
		GlobalScope.launch {
			val dao = AppDatabase.getAppDataBase(context = applicationContext)
				?.journalSquareDao()
			val squareList = dao?.getMeasurementById(commonMeasurements?.id ?: -1)
			val gson = Gson()
			val result = Intent()
			result.putExtra("EXTRA_SQUARE_RESULT", gson.toJson(squareList))
			setResult(Activity.RESULT_OK, result)
			finish()
		}
	}

}