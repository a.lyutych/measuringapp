package com.example.rangefinder.ui.measurement

enum class CrownFormHorizonal(val displayName: String) {
	CROWN_1("Округлые"),
	CROWN_2("Эллипсовидные"),
	CROWN_3("Однобокосжатые"),
	CROWN_4("Неправильные"),
}