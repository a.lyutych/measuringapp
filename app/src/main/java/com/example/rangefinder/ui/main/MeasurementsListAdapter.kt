package com.example.rangefinder.ui.main

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.MeasurementType
import kotlinx.android.synthetic.main.item_measurements_list.view.*

interface MeasurementsListCallback {

	fun onMeasurementsClick(commonMeasurements: CommonMeasurements)
	fun onMeasurementsLongClick(commonMeasurements: CommonMeasurements)
}

class MeasurementsListAdapter(
	private val callback: MeasurementsListCallback
) : RecyclerView.Adapter<MeasurementsListViewHolder>() {

	val data = arrayListOf<CommonMeasurements>()

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeasurementsListViewHolder {
		val view = LayoutInflater.from(parent.context).inflate(
			R.layout.item_measurements_list, parent, false)
		return MeasurementsListViewHolder(callback, view)
	}

	override fun onBindViewHolder(holder: MeasurementsListViewHolder, position: Int) {
		val measurements = data.getOrNull(position) ?: return
		holder.bind(measurements)
	}

	override fun getItemCount(): Int = data.size
}

class MeasurementsListViewHolder(
	private val callback: MeasurementsListCallback,
	view: View
) : RecyclerView.ViewHolder(view) {

	fun bind(measurements: CommonMeasurements) {
		itemView.setOnClickListener { callback.onMeasurementsClick(measurements) }
		itemView.setOnLongClickListener {  callback.onMeasurementsLongClick(measurements) ;
			return@setOnLongClickListener true }
		itemView.nameText.text = measurements.name
		val measurementType = MeasurementType.values()
			.getOrNull(measurements.numberOfMeasurementType ?: 0) ?: MeasurementType.HEIGHT
		itemView.typeNameText.text = itemView.context.getString(measurementType.typeNameResourceId)
	}
}