package com.example.rangefinder.ui.measurement

enum class InputMethod {

	BLUETOOTH, BY_HAND
}