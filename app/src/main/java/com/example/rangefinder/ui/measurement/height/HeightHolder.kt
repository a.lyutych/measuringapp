package com.example.rangefinder.ui.measurement.height

import android.R
import android.view.View
import android.widget.ArrayAdapter
import com.example.rangefinder.data.model.JournalHeightRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.*
import kotlinx.android.synthetic.main.item_height.view.*
import kotlinx.android.synthetic.main.item_diameter.view.breed
import kotlinx.android.synthetic.main.item_diameter.view.diameter
import kotlinx.android.synthetic.main.item_diameter.view.row_number

class HeightHolder(
	private val view: View,
	callback: MeasurementCallback? = null
) : MeasurementViewHolder(view, callback) {

	override fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		super.bind(measurement, adapter)
		val measurement = measurement as? JournalHeightRow ?: return

		view.row_number.text = adapterPosition.toString()
		measurement.breed?.let { view.breed.setText(it) } ?: view.breed.setText("")
		measurement.diameter?.let { view.diameter.setText(it.toString()) } ?: view.diameter.setText(
			""
		)
		measurement.height?.let { view.height_m.setText(it.toString()) }
			?: view.height_m.setText("")
		initTierSpinner(measurement)
		initGenerationSpinner(measurement)
		initAutoCompleteBreed()
	}

	override fun fillMeasurement(measurement: Measurement) {
		val measurement = measurement as? JournalHeightRow ?: return
		measurement.rowNumber = view.row_number.text.toString().toInt()
		measurement.tier = view.tierSpinner.selectedItem as? String
		measurement.generation = view.generationSpinner.selectedItem as? String
		measurement.breed = view.breed.text.toString()
		measurement.height = view.height_m.text.toString().toDoubleOrNull()
		measurement.diameter = view.diameter.text.toString().toDoubleOrNull()
	}

	private fun initTierSpinner(measurement: JournalHeightRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.tierSpinner.adapter = ArrayAdapter(
			view.context, R.layout.simple_spinner_item, tierNames
		)
		measurement.tier?.let {
			var selectedTierPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedTierPosition = index
			}
			view.tierSpinner.setSelection(selectedTierPosition)
		}
	}

	private fun initGenerationSpinner(measurement: JournalHeightRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.generationSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.generation?.let {
			var selectedGenerationPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedGenerationPosition = index
			}
			view.generationSpinner.setSelection(selectedGenerationPosition)
		}
	}

	private fun initAutoCompleteBreed() {
		val categoriesNames = TreeCodes.values().map { it.displayName }
		val adapter1 = ArrayAdapter(
			view.context, // Context
			android.R.layout.simple_dropdown_item_1line, // Layout
			categoriesNames // Array
		)
		view.breed.setAdapter(adapter1)
		view.breed.threshold = 1
		view.breed.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
			if (hasFocus) {
				view.breed.showDropDown()
			}
		}
	}
}