package com.example.rangefinder.ui.measurement.height

import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Switch
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.transfromer.JournalTreeCountRowTransformer
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.JournalHeightRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import com.example.rangefinder.ui.measurement.TreeCodes
import kotlinx.android.synthetic.main.fragment_height.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.sin

class HeightFragment : Fragment(), MeasurementCallback {

	private lateinit var adapter: MeasurementAdapter

	private var measurements: CommonMeasurements? = null

	private var listOfHeights = mutableListOf<Double?>()

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): HeightFragment =
			HeightFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		setHasOptionsMenu(true)
		return inflater.inflate(R.layout.fragment_height, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		initRecyclerView()
		initPreviousMeasurements()
		listenForUpdates()
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurements = AppDatabase.getAppDataBase(it)
						?.journalHeightRowDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurements != null) {
							adapter.data.addAll(measurements)
							adapter.data.add(JournalHeightRow())
						}
						else{
							showEmptyItem()
						}
						adapter.notifyDataSetChanged()
					}
				}
			}
		}
	}


	private fun listenForUpdates() {
		CommonInfo.instance.journalTreeCountRowLiveData.observe(viewLifecycleOwner, Observer {
			val journalHeightRow = JournalTreeCountRowTransformer().transform(it)
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val lastItem = adapter.data.last() as JournalHeightRow
			lastItem.apply {
				breed = TreeCodes.values().find {it.rawString == journalHeightRow.breed }?.displayName ?: journalHeightRow.breed
				diameter = journalHeightRow.diameter
			}
			adapter.data[adapter.data.lastIndex] = lastItem
			adapter.notifyDataSetChanged()
		})
		CommonInfo.instance.journalRowLiveData.observe(viewLifecycleOwner, Observer {
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val list = it.split(",").toList()
			val incline = list.getOrNull(6)?.toDoubleOrNull() ?: 0.0
			val inclineDistance = list.getOrNull(8)?.toDoubleOrNull() ?: 0.0
			val radians = Math.toRadians(incline)
			if (measurements?.isOneMeasurementHeight!!) {
				val height1 = inclineDistance.times(sin(radians))
				val heightResult =
					measurements?.initialHeight?.let { height -> height1.plus(height) }
				val lastItem = adapter.data.last() as JournalHeightRow
				lastItem.height = heightResult
				adapter.data[adapter.data.lastIndex] = lastItem
				adapter.notifyDataSetChanged()
				listOfHeights.clear()
			} else {
				val measureHeight = inclineDistance.times(sin(radians))
				listOfHeights.add(measureHeight)
				if (listOfHeights.size == 2) {
					val heightResult =
						listOfHeights.getOrNull(0)?.minus(listOfHeights.getOrNull(1) ?: 0.0)
					val lastItem = adapter.data.last() as JournalHeightRow
					lastItem.height = heightResult
					adapter.data[adapter.data.lastIndex] = lastItem
					adapter.notifyDataSetChanged()
					listOfHeights.clear()
				}
			}
		})
	}

	private fun initRecyclerView() {
		adapter = MeasurementAdapter(this)
		recyclerView.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@HeightFragment.adapter
		}
	}

	private fun showEmptyItem() {
		adapter.data.add(JournalHeightRow())
		adapter.notifyDataSetChanged()
	}

	override fun onDeleteClick(measurement: Measurement) {
		if (adapter.itemCount == 1) return

		val journalHeightRow = measurement as? JournalHeightRow ?: return
		GlobalScope.launch {
			context?.let {
				AppDatabase.getAppDataBase(it)
					?.journalHeightRowDao()
					?.deleteTableTreeCountRow(journalHeightRow)

				withContext(Dispatchers.Main) {
					adapter.data.remove(journalHeightRow)
					adapter.notifyDataSetChanged()
				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {
		val journalHeightRow = measurement as? JournalHeightRow ?: return
		measurements ?: return
		val test =TreeCodes.values().find {it.displayName == journalHeightRow.breed  }?.displayName ?: "not valid"
		if (test == journalHeightRow.breed) {
			GlobalScope.launch {
				context?.let {
					val id = AppDatabase.getAppDataBase(it)
						?.journalHeightRowDao()
						?.insertTableTreeCountRow(journalHeightRow.apply {
							measurementsId = measurements?.id ?: -1
						})
					journalHeightRow.id = id ?: -1

					withContext(Dispatchers.Main) {
						journalHeightRow.rowNumber?.let { adapter.data.set(it, journalHeightRow) }
						showEmptyItem()
						adapter.notifyDataSetChanged()
						recyclerView?.scrollToPosition(adapter.itemCount - 1)
					}
				}
			}
		}else{
			Toast.makeText(context, "Нет такой пароды! Измените", Toast.LENGTH_SHORT).show()
		}
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_height, menu)
		val heightSwitch = menu.findItem(R.id.menuHeightSwitch).actionView.findViewById<Switch>(
			R.id.heightSwitch
		)
		heightSwitch.setOnCheckedChangeListener { _, isChecked ->
			measurements?.isOneMeasurementHeight = !isChecked
		}
	}
}
