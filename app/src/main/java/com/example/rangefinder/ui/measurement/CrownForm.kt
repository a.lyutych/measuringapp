package com.example.rangefinder.ui.measurement

enum class CrownForm(val displayName: String) {
	CROWN_1("Конусовидные"),
	CROWN_2("Элипсовидныедные"),
	CROWN_3("Цилиндрические"),
	CROWN_4("Пароболоидные"),
	CROWN_5("Ромбовидные"),
	CROWN_6("Шаровидные"),
	CROWN_7("Сфероидальные"),
	CROWN_8("Плосковершинные"),
	CROWN_9("Куполообразные"),
	CROWN_10("Неправильные"),
	CROWN_11("Узорчатые"),
	CROWN_12("Раскидыстые"),
	CROWN_13("Сложные"),
}