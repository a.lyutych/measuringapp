package com.example.rangefinder.ui.measurement.treecount.setup

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import com.example.rangefinder.ui.measurement.InputMethod
import com.example.rangefinder.ui.measurement.treecount.TreeCountActivity

class TreeCountSetupActivity : BaseMeasurementSetupActivity() {

	override val measurementType: MeasurementType = MeasurementType.VALUE_OF_DIAMETER

	override val showSecondSpinner: Boolean = true

	override val showFirstSpinner: Boolean = false

	override fun saveGeodeticMeasurements(callback: (CommonMeasurements) -> Unit) =
		saveEmptyGeodeticMeasurements(callback)

	override fun launchMeasurementScreen(
		commonMeasurements: CommonMeasurements,
		inputMethod: InputMethod
	) {
		startActivityForResult(
			BaseMeasurementActivity.getLaunchIntent(
				this,
				commonMeasurements, TreeCountActivity::class.java, inputMethod
			), 1
		)
	}
	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (requestCode != BLUETOOTH_CODE) {
			setResult(Activity.RESULT_OK, data)
			finish()
			super.onActivityResult(requestCode, resultCode, data)
		} else {
			initDeviceSpinners()
		}
	}


	override fun getSetupContentFragment(): Fragment? = TreeCountSetupFragment()
}