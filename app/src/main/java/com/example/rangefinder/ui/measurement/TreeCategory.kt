package com.example.rangefinder.ui.measurement

enum class TreeCategory(val displayName: String) {
	CATEGORY_1("Деловые"),
	CATEGORY_2("Дровные"),
	CATEGORY_3("Сухостойные")

}