package com.example.rangefinder.ui.measurement

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.rangefinder.R
import com.example.rangefinder.data.model.*
import com.example.rangefinder.ui.measurement.circle.CircleHolder
import com.example.rangefinder.ui.measurement.geodetic.GeodeticHandViewHolder
import com.example.rangefinder.ui.measurement.squre.SquareHolder
import com.example.rangefinder.ui.measurement.geodetic.GeodeticViewHolder
import com.example.rangefinder.ui.measurement.height.HeightHolder
import com.example.rangefinder.ui.measurement.treecount.TreeCountHolder

class MeasurementAdapter(
	private val callback:
	MeasurementCallback? = null
) :
	RecyclerView.Adapter<MeasurementViewHolder>() {

	var data: MutableList<Measurement> = arrayListOf()
		set(value) {
			field = value
			notifyDataSetChanged()
		}

	companion object {
		const val TYPE_GEODETIC = R.layout.item_geodetic
		const val TYPE_VALUE_OF_DIAMETER = R.layout.item_diameter
		const val TYPE_RING_CHARACTERISTICS = R.layout.item_ring_characteristics
		const val TYPE_SQUARE = R.layout.item_square
		const val TYPE_HEIGHT = R.layout.item_height
		const val TYPE_GEODETIC_HAND = R.layout.geodetic_hand_row_item
		const val TYPE_UNDEFINED = -1
	}

	override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): MeasurementViewHolder {
		if (viewType == TYPE_UNDEFINED) throw IllegalArgumentException()
		val view = LayoutInflater.from(parent.context).inflate(viewType, parent, false)
		return when (viewType) {

			TYPE_GEODETIC -> GeodeticViewHolder(
				view,
				callback
			)
			TYPE_VALUE_OF_DIAMETER -> TreeCountHolder(view, callback)
			TYPE_HEIGHT -> HeightHolder(view, callback)
			TYPE_GEODETIC_HAND -> GeodeticHandViewHolder(view, callback)
			TYPE_RING_CHARACTERISTICS -> CircleHolder(view, callback)
			TYPE_SQUARE -> SquareHolder(view, callback)
			else -> throw IllegalArgumentException()
		}
	}

	override fun onBindViewHolder(holder: MeasurementViewHolder, position: Int) {
		holder.bind(data[position], this)
	}

	override fun getItemViewType(position: Int): Int {
		return when (data[position]) {
			is GeodeticHandRow -> TYPE_GEODETIC_HAND
			is JournalTreeCount -> TYPE_VALUE_OF_DIAMETER
			is GeodeticRow -> TYPE_GEODETIC
			is JournalRingCharacteristicsRow -> TYPE_RING_CHARACTERISTICS
			is JournalSquareRow -> TYPE_SQUARE
			is JournalHeightRow -> TYPE_HEIGHT
			else -> TYPE_UNDEFINED
		}
	}

	override fun getItemCount(): Int = data.size
}