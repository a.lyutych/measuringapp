package com.example.rangefinder.ui.measurement.geodetic

import android.view.View
import com.example.rangefinder.data.model.GeodeticRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import com.example.rangefinder.ui.measurement.MeasurementViewHolder
import kotlinx.android.synthetic.main.geodetic_hand_row_item.view.insideCorner
import kotlinx.android.synthetic.main.geodetic_hand_row_item.view.rhumb
import kotlinx.android.synthetic.main.geodetic_hand_row_item.view.slantDistance
import kotlinx.android.synthetic.main.geodetic_hand_row_item.view.verticalAngle
import kotlinx.android.synthetic.main.item_geodetic.view.*

class GeodeticViewHolder(
	private val view: View,
	callback: MeasurementCallback? = null
) : MeasurementViewHolder(view, callback) {

	override fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		super.bind(measurement, adapter)
		val journalRow = measurement as? GeodeticRow ?: return

		view.row_number.text = adapterPosition.toString()
		journalRow.bindingLine.let { view.binding_line.isChecked = it }
		journalRow.horizontalDistance?.let { view.horizontal_distance.setText(it.toString()) }
			?: view.horizontal_distance.setText("")
		journalRow.azimuth?.let { view.azimuth.setText(it.toString()) } ?: view.azimuth.setText("")
		journalRow.directionalAngle?.let { view.directional_angle.setText(it.toString()) }
			?: view.directional_angle.setText("")
		journalRow.coordinateX?.let { view.coordinateX.setText(it.toString()) } ?: view
			.coordinateX.setText("")
		journalRow.coordinateY?.let { view.coordinateY.setText(it.toString()) } ?: view
			.coordinateY.setText("")
		journalRow.rhumb?.let { view.rhumb.setText(it) } ?: view.rhumb.setText("")
		journalRow.insideCorner?.let { view.insideCorner.setText(it.toString()) } ?: view
			.insideCorner.setText("")
		journalRow.verticalAngle?.let { view.verticalAngle.setText(it.toString()) } ?: view
			.verticalAngle.setText("")
		journalRow.slantDistance?.let { view.slantDistance.setText(it.toString()) } ?: view
			.slantDistance.setText("")
		view.slantDistance.isEnabled = false
		view.verticalAngle.isEnabled = false
		//view.directional_angle.isEnabled = false
		view.rhumb.isEnabled = false
		view.insideCorner.isEnabled = false
	}

	override fun isValidInput(): Boolean {
		return !view.azimuth.text.isNullOrBlank()
				&& !view.directional_angle.text.isNullOrBlank()
				&& !view.horizontal_distance.text.isNullOrBlank()
				&& !view.coordinateX.text.isNullOrBlank()
				&& !view.coordinateY.text.isNullOrBlank()

	}

	override fun fillMeasurement(measurement: Measurement) {
		val measurement = measurement as? GeodeticRow ?: return
		measurement.rowNumber = view.row_number.text.toString().toIntOrNull()
		measurement.bindingLine = view.binding_line.isChecked
		measurement.horizontalDistance = view.horizontal_distance.text.toString().toDoubleOrNull()
		measurement.azimuth = view.azimuth.text.toString().toDoubleOrNull()
		measurement.directionalAngle = view.directional_angle.text.toString().toDoubleOrNull()
		measurement.coordinateX = view.coordinateX.text.toString().toDoubleOrNull()
		measurement.coordinateY = view.coordinateY.text.toString().toDoubleOrNull()
		measurement.rhumb = view.rhumb.text.toString()
		measurement.insideCorner = view.insideCorner.text.toString().toDoubleOrNull()
		measurement.slantDistance = view.slantDistance.text.toString().toDoubleOrNull()
		measurement.verticalAngle = view.verticalAngle.text.toString().toDoubleOrNull()
}
}