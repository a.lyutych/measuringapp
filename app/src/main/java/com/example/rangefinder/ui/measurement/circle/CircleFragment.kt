package com.example.rangefinder.ui.measurement.circle

import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Switch
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.transfromer.JournalTreeCountRowTransformer
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.JournalRingCharacteristicsRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import com.example.rangefinder.ui.measurement.TreeCodes
import kotlinx.android.synthetic.main.fragment_circle.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.sin

class CircleFragment : Fragment(), MeasurementCallback {
	private lateinit var adapter: MeasurementAdapter

	private var measurements: CommonMeasurements? = null
	private var listOfHeights = mutableListOf<Double?>()
	private var selectedField: String = "D"

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): CircleFragment =
			CircleFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		setHasOptionsMenu(true)
		return inflater.inflate(R.layout.fragment_circle, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		initRecyclerView()
		initPreviousMeasurements()
		listenForUpdates()
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurements = AppDatabase.getAppDataBase(it)
						?.journalCircleDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurements != null) {
							adapter.data.addAll(measurements)
							adapter.data.add(JournalRingCharacteristicsRow())
						}
						else{
							showEmptyItem()
						}
						adapter.notifyDataSetChanged()
					}
				}
			}
		}
	}

	private fun listenForUpdates() {
		CommonInfo.instance.journalTreeCountRowLiveData.observe(viewLifecycleOwner, Observer {
			val journalTreeCount = JournalTreeCountRowTransformer().transform(it)
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val lastItem = adapter.data.last() as JournalRingCharacteristicsRow
			lastItem.apply {

				breed = TreeCodes.values().find {it.rawString == journalTreeCount.breed }?.displayName ?: journalTreeCount.breed
				diameter = journalTreeCount.diameter
			}
			adapter.data[adapter.data.lastIndex] = lastItem
			adapter.notifyDataSetChanged()
		})
		CommonInfo.instance.journalRowLiveData.observe(viewLifecycleOwner, Observer {
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val list = it.split(",").toList()
			when (selectedField) {
				"D" -> setDistanceAndIncline(list)
				"H" -> setHeight(list)
			}
		})
	}

	private fun setDistanceAndIncline(list: List<String>) {
		val azimuth = list.getOrNull(2)?.toDoubleOrNull()
		val measureHD = list.getOrNull(4)?.toDoubleOrNull()
		val lastItem = adapter.data.last() as JournalRingCharacteristicsRow
		lastItem.apply {
			direction = measurements?.magneticDeclination?.let { azimuth?.plus(it) }
			horizontalDistance = measureHD
		}
		adapter.data[adapter.data.lastIndex] = lastItem
		adapter.notifyDataSetChanged()
		listOfHeights.clear()
	}

	private fun setHeight(list: List<String>) {
		val incline = list.getOrNull(6)?.toDoubleOrNull() ?: 0.0
		val inclineDistance = list.getOrNull(8)?.toDoubleOrNull() ?: 0.0
		val radianIncline = Math.toRadians(incline)
		if (measurements?.isOneMeasurementHeight!!) {
			val height1 = inclineDistance.times(sin(radianIncline))
			val heightResult =
				measurements?.initialHeight?.let { height -> height1.plus(height) }
			insertHeightInRow(heightResult)
		} else {
			val measureHeight = inclineDistance.times(sin(radianIncline))
			listOfHeights.add(measureHeight)
			if (listOfHeights.size == 2) {
				val heightResult =
					listOfHeights.getOrNull(0)?.minus(listOfHeights.getOrNull(1) ?: 0.0)
				insertHeightInRow(heightResult)
			}
		}
	}

	private fun insertHeightInRow(heightResult: Double?) {
		val lastItem = adapter.data.last() as JournalRingCharacteristicsRow
		lastItem.height = heightResult
		adapter.data[adapter.data.lastIndex] = lastItem
		adapter.notifyDataSetChanged()
		listOfHeights.clear()
	}

	private fun initRecyclerView() {
		adapter = MeasurementAdapter(this)
		recyclerViewCircle.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@CircleFragment.adapter
		}

	}

	private fun showEmptyItem() {
		adapter.data.add(JournalRingCharacteristicsRow())
		adapter.notifyDataSetChanged()
	}

	override fun onDeleteClick(measurement: Measurement) {
		if (adapter.itemCount == 1) return

		val journalCircle = measurement as? JournalRingCharacteristicsRow ?: return
		GlobalScope.launch {
			context?.let {
				AppDatabase.getAppDataBase(it)
					?.journalCircleDao()
					?.deleteCircleRow(journalCircle)

				withContext(Dispatchers.Main) {
					adapter.data.remove(journalCircle)
					adapter.notifyDataSetChanged()
				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {
		val journalCircle = measurement as? JournalRingCharacteristicsRow ?: return
		measurements ?: return
		val test =TreeCodes.values().find {it.displayName == journalCircle.breed  }?.displayName ?: "not valid"
		if (test == journalCircle.breed) {
			GlobalScope.launch {
				context?.let {
					val id = AppDatabase.getAppDataBase(it)
						?.journalCircleDao()
						?.insertCircleRow(journalCircle.apply {
							measurementsId = measurements?.id ?: -1
						})
					journalCircle.id = id ?: -1

					withContext(Dispatchers.Main) {
						journalCircle.rowNumber?.let { adapter.data.set(it, journalCircle) }
						showEmptyItem()
						adapter.notifyDataSetChanged()
						recyclerViewCircle?.scrollToPosition(adapter.itemCount - 1)
					}
				}
			}
		}
		else{
			Toast.makeText(context, "Нет такой пароды! Измените", Toast.LENGTH_SHORT).show()
		}
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_height, menu)
		val heightSwitch = menu.findItem(R.id.menuHeightSwitch).actionView.findViewById<Switch>(
			R.id
				.heightSwitch
		)
		heightSwitch.setOnCheckedChangeListener { _, isChecked ->
			measurements?.isOneMeasurementHeight = !isChecked
		}
	}

	override fun onFieldSelect(field: String) {
		selectedField = field
	}
}