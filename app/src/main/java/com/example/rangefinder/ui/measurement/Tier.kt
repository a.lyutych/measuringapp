package com.example.rangefinder.ui.measurement

enum class Tier (val displayName: String) {
	TIER_0("0"),
	TIER_1("1"),
	TIER_2("2"),
	TIER_3("3"),
}