package com.example.rangefinder.ui.measurement.squre

import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.*
import android.widget.Switch
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.transfromer.JournalTreeCountRowTransformer
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.JournalSquareRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import com.example.rangefinder.ui.measurement.TreeCodes
import kotlinx.android.synthetic.main.fragment_square.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.sin

class SquareFragment : Fragment(), MeasurementCallback {
	private lateinit var adapter: MeasurementAdapter
	private var heightSelected = "H1"
	private var diameterSelected = "D1"
	private var listOfHeights = mutableListOf<Double?>()

	private var measurements: CommonMeasurements? = null

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): SquareFragment =
			SquareFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		setHasOptionsMenu(true)
		return inflater.inflate(R.layout.fragment_square, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		initRecyclerView()
		initPreviousMeasurements()
		listenForUpdates()
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurements = AppDatabase.getAppDataBase(it)
						?.journalSquareDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurements != null) {
							adapter.data.addAll(measurements)
							adapter.data.add(JournalSquareRow())
						} else {
							showEmptyItem()
						}
						adapter.notifyDataSetChanged()
					}
				}
			}
		}
	}

	private fun listenForUpdates() {
		CommonInfo.instance.journalTreeCountRowLiveData.observe(viewLifecycleOwner, Observer {
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val journalTreeCount = JournalTreeCountRowTransformer().transform(it)
			val lastItem = adapter.data.last() as JournalSquareRow
			lastItem.apply {
				breed =
					TreeCodes.values().find { it.rawString == journalTreeCount.breed }?.displayName
						?: journalTreeCount.breed
			}
			when (diameterSelected) {
				"D1" -> lastItem.diameterOne = journalTreeCount.diameter
				"D2" -> lastItem.diameterTwo = journalTreeCount.diameter
				else -> return@Observer
			}
			adapter.data[adapter.data.lastIndex] = lastItem
			adapter.notifyDataSetChanged()
		})
		CommonInfo.instance.journalRowLiveData.observe(viewLifecycleOwner, Observer {
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val list = it.split(",").toList()
			val incline = list.getOrNull(6)?.toDoubleOrNull() ?: 0.0
			val inclineDistance = list.getOrNull(8)?.toDoubleOrNull() ?: 0.0
			val inclineRadian = Math.toRadians(incline)
			if (measurements?.isOneMeasurementHeight!!) {
				val height1 = inclineDistance.times(sin(inclineRadian))
				val heightResult =
					measurements?.initialHeight?.let { height -> height1.plus(height) }
				insertHeightInRow(heightResult)
			} else {
				val measureHeight = inclineDistance.times(sin(inclineRadian))
				listOfHeights.add(measureHeight)
				if (listOfHeights.size == 2) {
					val heightResult =
						listOfHeights.getOrNull(0)?.minus(listOfHeights.getOrNull(1) ?: 0.0)
					insertHeightInRow(heightResult)
				}
			}
		})
	}

	private fun insertHeightInRow(heightResult: Double?) {
		val lastItem = adapter.data.last() as JournalSquareRow
		when (heightSelected) {
			"H1" -> lastItem.height = heightResult
			"H2" -> lastItem.heightTwo = heightResult
			"H3" -> lastItem.heightThree = heightResult
			else -> return
		}
		adapter.data[adapter.data.lastIndex] = lastItem
		adapter.notifyDataSetChanged()
		listOfHeights.clear()
	}

	private fun initRecyclerView() {
		adapter = MeasurementAdapter(this)
		recyclerViewSquare.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@SquareFragment.adapter
		}
	}

	private fun showEmptyItem() {
		adapter.data.add(JournalSquareRow())
		adapter.notifyDataSetChanged()
	}

	override fun onDeleteClick(measurement: Measurement) {
		if (adapter.itemCount == 1) return

		val journalSquare = measurement as? JournalSquareRow ?: return
		GlobalScope.launch {
			context?.let {
				AppDatabase.getAppDataBase(it)
					?.journalSquareDao()
					?.deleteSquareRow(journalSquare)

				withContext(Dispatchers.Main) {
					adapter.data.remove(journalSquare)
					adapter.notifyDataSetChanged()
				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {
		val journalSquare = measurement as? JournalSquareRow ?: return
		measurements ?: return
		val test = TreeCodes.values().find { it.displayName == journalSquare.breed }?.displayName
			?: "not valid"
		if (test == journalSquare.breed) {
			GlobalScope.launch {
				context?.let {
					val id = AppDatabase.getAppDataBase(it)
						?.journalSquareDao()
						?.insertSquareRow(journalSquare.apply {
							measurementsId = measurements?.id ?: -1
						})
					journalSquare.id = id ?: -1

					withContext(Dispatchers.Main) {
						journalSquare.rowNumber?.let { adapter.data.set(it, journalSquare) }
						showEmptyItem()
						adapter.notifyDataSetChanged()
						recyclerViewSquare?.scrollToPosition(adapter.itemCount - 1)
					}
				}
			}
		} else {
			Toast.makeText(context, "Нет такой пароды! Измените", Toast.LENGTH_SHORT).show()
		}

	}

	override fun onFieldSelect(field: String) {
		when (field) {
			"H1", "H2", "H3" -> heightSelected = field
			"D1", "D2" -> diameterSelected = field
		}
	}

	override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
		super.onCreateOptionsMenu(menu, inflater);
		inflater.inflate(R.menu.menu_height, menu)
		val heightSwitch = menu.findItem(R.id.menuHeightSwitch).actionView.findViewById<Switch>(
			R.id.heightSwitch
		)
		heightSwitch.setOnCheckedChangeListener { _, isChecked ->
			measurements?.isOneMeasurementHeight = !isChecked
		}
	}
}