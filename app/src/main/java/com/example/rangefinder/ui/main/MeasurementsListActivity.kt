package com.example.rangefinder.ui.main

import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.R
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.main.createmeasurement.CreateMeasurementActivity
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import kotlinx.android.synthetic.main.activity_measurements_list.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
 var isLight = false
class MeasurementsListActivity : AppCompatActivity(), MeasurementsListCallback {
	private lateinit var pref: SharedPreferences
	 val APP_PREFERENCES = "mysettings"

	private val adapter by lazy { MeasurementsListAdapter(this) }

	override fun onCreate(savedInstanceState: Bundle?) {
		pref = getSharedPreferences(APP_PREFERENCES, MODE_PRIVATE)
		isLight = pref.getBoolean(APP_PREFERENCES,false)
		if (isLight)setTheme(R.style.AppThemeLight)
		else setTheme(R.style.AppTheme)

		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_measurements_list)

		initRecyclerView()

		createMeasurementButton?.setOnClickListener {
			startActivity(Intent(this, CreateMeasurementActivity::class.java))
		}

	}
	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		// Inflate the menu; this adds items to the action bar if it is present.
		menuInflater.inflate(R.menu.menu_main, menu)
		return true
	}


	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		// Handle action bar item clicks here.
		val id = item.itemId

		if (id == R.id.switchTheme) {
			isLight = !isLight
			val editor = pref.edit()
			editor.putBoolean(APP_PREFERENCES,isLight)
			editor.apply()
			this.finish();
			this.startActivity(intent);
			return true;
		}


		return super.onOptionsItemSelected(item)

	}
	override fun onStart() {
		super.onStart()
		showMeasurements()
	}

	override fun onMeasurementsClick(commonMeasurements: CommonMeasurements) {
		val measurementType =
			MeasurementType.values()[commonMeasurements.numberOfMeasurementType ?: 0]
		startActivity(
			BaseMeasurementSetupActivity.getLaunchIntent(
				this,
				measurementType,
				commonMeasurements.name
			)
		)
	}

	override fun onMeasurementsLongClick(commonMeasurements: CommonMeasurements) {
		val builder = AlertDialog.Builder(this)

		with(builder)
		{
			setTitle("Предупреждение")
			setMessage("Вы точно хотите удалить замеры ${commonMeasurements.name} ?")
			setPositiveButton("Да") { dialog, whichButton ->
				deleteMeasurement(commonMeasurements)
				dialog.dismiss()
			}
			setNegativeButton("Нет") { dialog, whichButton ->
				dialog.dismiss()
			}

			show()
		}
	}

	private fun deleteMeasurement(commonMeasurements: CommonMeasurements) {

		GlobalScope.launch {
			AppDatabase.getAppDataBase(this@MeasurementsListActivity)
				?.commonMeasurementsDao()?.deleteMeasurements(commonMeasurements)
			withContext(Dispatchers.Main) {
				adapter.data.remove(commonMeasurements)
				adapter.notifyDataSetChanged()
			}
		}

	}

	private fun showMeasurements() {
		adapter.data.clear()

		GlobalScope.launch {
			val measurements =
				AppDatabase.getAppDataBase(this@MeasurementsListActivity)
					?.commonMeasurementsDao()?.getMeasurements()

			withContext(Dispatchers.Main) {
				adapter.data.addAll(
					measurements?.filter { !it.name.isBlank() } ?: arrayListOf()
				)
				adapter.notifyDataSetChanged()
			}
		}
	}

	private fun initRecyclerView() {
		recyclerView?.apply {
			layoutManager = LinearLayoutManager(this@MeasurementsListActivity)
			adapter = this@MeasurementsListActivity.adapter
		}
	}
}