package com.example.rangefinder.ui.measurement.circle.setup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import kotlinx.android.synthetic.main.fragment_circle_setup.*

class CircleSetupFragment : Fragment() {

	private var measurements: CommonMeasurements? = null

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_circle_setup, container, false)
	}
	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		measurements = arguments?.getSerializable(BaseMeasurementSetupActivity.EXTRA_EXISTING_MEASUREMENTS)
				as? CommonMeasurements
		measurements?.let {
			declensionEdit.setText(measurements?.magneticDeclination.toString())
			declensionEdit.isEnabled =false
			initial_height.setText(measurements?.initialHeight.toString())
			initial_height.isEnabled=false
		}
		super.onViewCreated(view, savedInstanceState)
	}
}