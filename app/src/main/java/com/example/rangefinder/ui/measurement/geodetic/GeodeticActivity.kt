package com.example.rangefinder.ui.measurement.geodetic

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AlertDialog
import com.example.rangefinder.R
import com.example.rangefinder.data.dto.GeodeticDto
import com.example.rangefinder.data.dto.GeodeticRowDto
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.GeodeticHandRow
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.GeodeticRow
import com.example.rangefinder.roundTo5DecimalPlaces
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.InputMethod
import com.google.gson.Gson
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlin.math.cos
import kotlin.math.pow
import kotlin.math.sin
import kotlin.math.sqrt

class GeodeticActivity : BaseMeasurementActivity() {

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_geodetic)
		attachContentFragment(commonMeasurements)
	}

	private fun attachContentFragment(commonMeasurements: CommonMeasurements?) {
		if (commonMeasurements == null) return
		if (inputMethod == InputMethod.BY_HAND) {
			supportFragmentManager.beginTransaction()
				.replace(R.id.fragmentFrame, GeodeticHandFragment.newInstance(commonMeasurements))
				.commit()
		} else {
			supportFragmentManager.beginTransaction()
				.replace(R.id.fragmentFrame, GeodeticFragment.newInstance(commonMeasurements))
				.commit()
		}
	}

	override fun onCreateOptionsMenu(menu: Menu): Boolean {
		menuInflater.inflate(R.menu.menu_geodetic, menu)
		return true
	}

	override fun onOptionsItemSelected(item: MenuItem): Boolean {
		when (item.itemId) {
			R.id.linkage -> {
				if (inputMethod == InputMethod.BY_HAND)
					handLinkage()
				else linkage()
				return true
			}
			R.id.exitGeodetic -> {
				val builder = AlertDialog.Builder(this)

				with(builder)
				{
					setTitle("Предупреждение")
					setMessage("Вы точно хотите завершить измерения ?")
					setPositiveButton("Да") { dialog, _ ->
						closeApp()
						dialog.dismiss()
					}
					setNegativeButton("Нет") { dialog, _ ->
						dialog.dismiss()
					}

					show()
				}
				return true
			}
		}
		return super.onOptionsItemSelected(item)
	}

	private fun linkage(){
		GlobalScope.launch {
			val dao = AppDatabase.getAppDataBase(context = applicationContext)
				?.journalRowDao()
			val geodeticList = dao?.getMeasurementById(commonMeasurements?.id ?: -1)
			val geodeticClosedRun = geodeticList
				?.filter { it.bindingLine }
				?.sortedBy { it.rowNumber }?.toMutableList()
			if (geodeticClosedRun != null && geodeticClosedRun.isNotEmpty()) {
				geodeticClosedRun.add(calculateLastRow(geodeticClosedRun))
				val prelast = geodeticClosedRun[geodeticClosedRun.lastIndex-1]
				val a = prelast.horizontalDistance ?: 0.0
				val b1 = prelast.coordinateX?.minus(
					geodeticClosedRun.first().coordinateX
						?: 0.0
				)?.let { it.pow(2) }
				val b2 = prelast.coordinateY?.minus(
					geodeticClosedRun.first().coordinateY
						?: 0.0
				)?.let { it.pow(2) }
				val b = b1?.plus(b2 ?: 0.0)?.let { sqrt(it) } ?: 0.0
				val c1 = geodeticClosedRun.last().coordinateX?.minus(
					geodeticClosedRun.first()
						.coordinateX
						?: 0.0
				)?.let { it.pow(2) }
				val c2 = geodeticClosedRun.last().coordinateY?.minus(
					geodeticClosedRun.first().coordinateY
						?: 0.0
				)?.let { it.pow(2) }
				val c = c1?.plus(c2 ?: 0.0)?.let { sqrt(it) } ?: 0.0
				val sumLine = geodeticClosedRun.mapNotNull { it.horizontalDistance }.sum()
				val cc = c.div(sumLine)
				val angleR = (a.pow(2).plus(b.pow(2).plus(c.pow(2)))).div(2.times(a).times(b))
				val horizontalLayings = geodeticClosedRun.map { it.horizontalDistance ?: 0.0 }
				val availableLineValue = horizontalLayings.sum().div(200)
				var fx = 0.0
				var fy = 0.0
				for (i in 1 until geodeticClosedRun.size) {
					val dX = geodeticClosedRun[i].coordinateX?.minus(
						geodeticClosedRun[i - 1].coordinateX ?: 0.0
					)
					fx = fx.plus(dX ?: 0.0)
					val dY = geodeticClosedRun[i].coordinateY?.minus(
						geodeticClosedRun[i - 1].coordinateY ?: 0.0
					)
					fy = fy.plus(dY ?: 0.0)
				}
				val avaialbleAngle = 1.times(sqrt(geodeticClosedRun.size.toDouble()))
				val absoluteResidual = sqrt(fx.pow(2).plus(fy.pow(2)))
				val isAngleResidual = avaialbleAngle > angleR
				val isLineResidual = cc < availableLineValue
				val kx = (0.minus(fx)).div(horizontalLayings.sum())
				val ky = (0.minus(fy)).div(horizontalLayings.sum())
				val message = "Угловая невязка  ${angleR.roundTo5DecimalPlaces()}\n" +
						"Допустимая ${avaialbleAngle.roundTo5DecimalPlaces()}\n " +
						"Линейная невязка ${cc.roundTo5DecimalPlaces()}\n " +
						"Допустимая ${availableLineValue.roundTo5DecimalPlaces()}\n " +
						"Площадь  ${getSquare(geodeticClosedRun)}"
				runOnUiThread {
					val builder = AlertDialog.Builder(this@GeodeticActivity)
					with(builder)
					{
						setTitle("Невязка")
						setMessage(message)
						if (isAngleResidual && isLineResidual) {
							setPositiveButton("Увязать") { dialog, _ ->
								GlobalScope.launch {
									val anglePart = angleR.div(geodeticClosedRun.size)
									geodeticClosedRun.forEach {
										recalculateAngleForRow(
											it,
											anglePart
										)
									}
									geodeticClosedRun.forEach {
										it.coordinateX?.plus(kx)
										it.coordinateY?.plus(ky)
									}
									dao.updateGeodeticRows(geodeticClosedRun)
								}
								dialog.dismiss()
								closeApp()
							}
						}
						setNegativeButton("Отмена") { dialog, _ ->
							dialog.dismiss()
						}

						show()
					}
				}
			}
		}
	}

	private fun handLinkage() {
			GlobalScope.launch {
				val dao = AppDatabase.getAppDataBase(context = applicationContext)
					?.geodeticHandRowDao()
				val geodeticList = dao?.getMeasurementById(commonMeasurements?.id ?: -1)
				val geodeticClosedRun = geodeticList
					?.filter { it.bindingLine }
					?.sortedBy { it.rowNumber }?.toMutableList()
				if (geodeticClosedRun != null && geodeticClosedRun.isNotEmpty()) {
					geodeticClosedRun.add(calculateLastRow(geodeticClosedRun))
					val prelast = geodeticClosedRun[geodeticClosedRun.lastIndex-1]
					val a = prelast.horizontalDistance ?: 0.0
					val b1 = prelast.coordinateX?.minus(
						geodeticClosedRun.first().coordinateX
							?: 0.0
					)?.let { it.pow(2) }
					val b2 = prelast.coordinateY?.minus(
						geodeticClosedRun.first().coordinateY
							?: 0.0
					)?.let { it.pow(2) }
					val b = b1?.plus(b2 ?: 0.0)?.let { sqrt(it) } ?: 0.0
					val c1 = geodeticClosedRun.last().coordinateX?.minus(
						geodeticClosedRun.first()
							.coordinateX
							?: 0.0
					)?.let { it.pow(2) }
					val c2 = geodeticClosedRun.last().coordinateY?.minus(
						geodeticClosedRun.first().coordinateY
							?: 0.0
					)?.let { it.pow(2) }
					val c = c1?.plus(c2 ?: 0.0)?.let { sqrt(it) } ?: 0.0
					val sumLine = geodeticClosedRun.mapNotNull { it.horizontalDistance }.sum()
					val cc = c.div(sumLine)
					val angleR = (a.pow(2).plus(b.pow(2).plus(c.pow(2)))).div(2.times(a).times(b))
					val horizontalLayings = geodeticClosedRun.map { it.horizontalDistance ?: 0.0 }
					val availableLineValue = horizontalLayings.sum().div(200)
					var fx = 0.0
					var fy = 0.0
					for (i in 1 until geodeticClosedRun.size) {
						val dX = geodeticClosedRun[i].coordinateX?.minus(
							geodeticClosedRun[i - 1].coordinateX ?: 0.0
						)
						fx = fx.plus(dX ?: 0.0)
						val dY = geodeticClosedRun[i].coordinateY?.minus(
							geodeticClosedRun[i - 1].coordinateY ?: 0.0
						)
						fy = fy.plus(dY ?: 0.0)
					}
					val avaialbleAngle = 1.times(sqrt(geodeticClosedRun.size.toDouble()))
					val isAngleResidual = avaialbleAngle > angleR
					val isLineResidual = cc < availableLineValue
					val kx = (0.minus(fx)).div(horizontalLayings.sum())
					val ky = (0.minus(fy)).div(horizontalLayings.sum())
					val message = "Угловая невязка  ${angleR.roundTo5DecimalPlaces()}\n" +
							"Допустимая ${avaialbleAngle.roundTo5DecimalPlaces()}\n " +
							"Линейная невязка ${cc.roundTo5DecimalPlaces()}\n " +
							"Допустимая ${availableLineValue.roundTo5DecimalPlaces()}\n " +
							"Площадь  ${getSquare(geodeticClosedRun)}"
					runOnUiThread {
						val builder = AlertDialog.Builder(this@GeodeticActivity)
						with(builder)
						{
							setTitle("Невязка")
							setMessage(message)
							if (isAngleResidual && isLineResidual) {
								setPositiveButton("Увязать") { dialog, _ ->
									GlobalScope.launch {
										val anglePart = angleR.div(geodeticClosedRun.size)
										geodeticClosedRun.forEach {
											recalculateAngleForRow(
												it,
												anglePart
											)
										}
										geodeticClosedRun.forEach {
											it.coordinateX?.plus(kx)
											it.coordinateY?.plus(ky)
										}
										dao.updateGeodeticHandRows(geodeticClosedRun)
									}
									closeApp()
									dialog.dismiss()
								}
							}
							setNegativeButton("Отмена") { dialog, _ ->
								dialog.dismiss()
							}
							show()
						}
					}
				}
			}
	}

	private fun calculateLastRow(geodeticClosedRun: MutableList<GeodeticHandRow>): GeodeticHandRow {
		val addLastCoordiantesRow = GeodeticHandRow()
		addLastCoordiantesRow.bindingLine = true
		val lastGeodetic = geodeticClosedRun.last()
		val radianDirection = lastGeodetic.directionalAngle ?: 0.0
		val deltaX =
			lastGeodetic.horizontalDistance?.times(cos(Math.toRadians(radianDirection))) ?: 0.0
		addLastCoordiantesRow.coordinateX =
			lastGeodetic.coordinateX?.plus(deltaX)?.roundTo5DecimalPlaces()
		addLastCoordiantesRow.rowNumber = lastGeodetic.rowNumber?.plus(1)
		val deltaY =
			lastGeodetic.horizontalDistance?.times(sin(Math.toRadians(radianDirection))) ?: 0.0
		addLastCoordiantesRow.coordinateY =
			lastGeodetic.coordinateY?.plus(deltaY)?.roundTo5DecimalPlaces()
		addLastCoordiantesRow.measurementsId = lastGeodetic.measurementsId
		return addLastCoordiantesRow
	}

	private fun calculateLastRow(geodeticClosedRun: MutableList<GeodeticRow>): GeodeticRow {
		val addLastCoordiantesRow = GeodeticRow()
		addLastCoordiantesRow.bindingLine = true
		val lastGeodetic = geodeticClosedRun.last()
		val radianDirection = lastGeodetic.directionalAngle ?: 0.0
		val deltaX =
			lastGeodetic.horizontalDistance?.times(cos(Math.toRadians(radianDirection))) ?: 0.0
		addLastCoordiantesRow.coordinateX = lastGeodetic.coordinateX?.plus(deltaX)
		addLastCoordiantesRow.rowNumber = lastGeodetic.rowNumber?.plus(1)
		val deltaY = lastGeodetic.horizontalDistance?.times(sin(Math.toRadians(radianDirection))) ?: 0.0
		addLastCoordiantesRow.coordinateY = lastGeodetic.coordinateY?.plus(deltaY)
		addLastCoordiantesRow.measurementsId = lastGeodetic.measurementsId
		return addLastCoordiantesRow
	}

	private fun recalculateAngleForRow(row: GeodeticRow, anglePart: Double) {
		row.insideCorner = row.insideCorner?.minus(anglePart)
		//add if necessary
	}

	private fun getSquare(rowList: List<GeodeticRow>): Double {
		var firstPart = 0.0
		var secondPart = 0.0
		for (i in 1 until rowList.size) {
			val x = rowList[i].coordinateX ?: 0.0
			val y = rowList[i].coordinateY ?: 0.0
			val deltaX = x.minus(rowList[i - 1].coordinateX ?: 0.0)
			val deltaY = y.minus(rowList[i - 1].coordinateY ?: 0.0)
			firstPart = firstPart.plus(x.times(deltaY))
			secondPart = secondPart.plus(y.times(deltaX))
		}
		val doubleSquare = firstPart.minus(secondPart)
		val realSquare = doubleSquare.div(2)
		return if (realSquare < 0) 0.0 else realSquare
	}

	private fun closeApp() {
		GlobalScope.launch {
			val db = AppDatabase.getAppDataBase(context = applicationContext)
			val result = Intent()
			var dto = GeodeticDto()
			if (inputMethod == InputMethod.BY_HAND) {
				val list = db?.geodeticHandRowDao()?.getMeasurementById(
					commonMeasurements?.id ?: -1
				)
				dto = transformToDto(list)
			} else {
				val list = db?.journalRowDao()?.getMeasurementById(
					commonMeasurements?.id ?: -1
				)
				dto = transformToDto(list)
			}
			val gson = Gson()
			result.putExtra(EXTRA_HAND_GEODETIC_RESULT, gson.toJson(dto))
			setResult(Activity.RESULT_OK, result)
			finish()
		}
	}

	private fun transformToDto(list: List<GeodeticRow>?): GeodeticDto {
		val dto = GeodeticDto()
		val geodeticClosedRun = list
			?.filter { it.bindingLine && it.insideCorner != null }
		val rowsToSend = mutableListOf<GeodeticRowDto>()
		list?.forEach {
			rowsToSend.add(GeodeticRowDto().apply {
				coordinateX = it.coordinateX
				coordinateY = it.coordinateY
				directionAngle = it.directionalAngle
				rhumb = it.rhumb
				isBindingLine = it.bindingLine
				insideAngle = it.insideCorner
				horizontalDistance = it.horizontalDistance
				rowNumber = it.rowNumber
				slantDistance = it.slantDistance
				verticalAngle = it.verticalAngle
				azimuth = it.azimuth
			})
		}
		dto.apply {
			travelDirection = commonMeasurements?.travelDirection
			firstPointCoordinateX = commonMeasurements?.firstPointCoordinateX
			firstPointCoordinateY = commonMeasurements?.firstPointCoordinateY
			square = geodeticClosedRun?.let { getSquare(it) }
			rows = rowsToSend
		}
		return dto
	}

	companion object {
		const val EXTRA_HAND_GEODETIC_RESULT = "EXTRA_HAND_GEODETIC_RESULT"
	}
}
