package com.example.rangefinder.ui.measurement.geodetic

import android.media.RingtoneManager
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.CommonInfo
import com.example.rangefinder.R
import com.example.rangefinder.data.bluetooth.transfromer.JournalRowTransformer
import com.example.rangefinder.data.helper.MathHelper
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.GeodeticRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.roundTo1DecimalPlaces
import com.example.rangefinder.roundTo5DecimalPlaces
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import kotlinx.android.synthetic.main.fragment_geodetic.*
import kotlinx.android.synthetic.main.item_geodetic.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.cos
import kotlin.math.sin

class GeodeticFragment : Fragment(), MeasurementCallback {

	private lateinit var viewAdapter: MeasurementAdapter
	private var lastRow = GeodeticRow()
	private var previousDirectionalAngle: Double? = null

	private var measurements: CommonMeasurements? = null

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): GeodeticFragment =
			GeodeticFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_geodetic, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		viewAdapter = MeasurementAdapter(this)


		journalLayout.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@GeodeticFragment.viewAdapter
		}
		initPreviousMeasurements()
		CommonInfo.instance.journalRowLiveData.observe(viewLifecycleOwner, Observer {
			try {
				val notify: Uri =
					RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
				val r = RingtoneManager.getRingtone(context, notify)
				r.play()
			} catch (e: Exception) {
				e.printStackTrace()
			}
			val journalRowDto = JournalRowTransformer().transform(it)
			lastRow.azimuth = journalRowDto.azimuth
			lastRow.horizontalDistance = journalRowDto.horizontalDistance
			lastRow.slantDistance = journalRowDto.slantDistance
			lastRow.verticalAngle = journalRowDto.verticalAngle
			lastRow.directionalAngle = lastRow.azimuth?.plus(measurements?.magneticDeclination!!)
			lastRow.rhumb = MathHelper.fromAzimuthToRhumb(lastRow.azimuth?.roundTo1DecimalPlaces())
			lastRow.insideCorner = MathHelper.fromDirectionalToInside(
				measurements!!.travelDirection,
				lastRow.directionalAngle, previousDirectionalAngle
			)
			viewAdapter.data[viewAdapter.data.lastIndex] = lastRow
			viewAdapter.notifyDataSetChanged()
		})
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurement = AppDatabase.getAppDataBase(it)
						?.journalRowDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurement != null && measurement.isNotEmpty()) {
							viewAdapter.data.addAll(measurement)
							val addLastCoordiantesRow = GeodeticRow()
							val lastGeodetic = measurement.last()
							val radianDirection = lastGeodetic.directionalAngle ?: 0.0
							val deltaX =
								lastGeodetic.horizontalDistance?.times(
									cos(
										Math.toRadians(
											radianDirection
										)
									)
								) ?: 0.0
							addLastCoordiantesRow.coordinateX =
								lastGeodetic.coordinateX?.plus(deltaX)?.roundTo5DecimalPlaces()
							addLastCoordiantesRow.rowNumber = lastGeodetic.rowNumber?.plus(1)
							val deltaY = lastGeodetic.horizontalDistance?.times(
								sin(
									Math.toRadians(radianDirection)
								)
							) ?: 0.0
							addLastCoordiantesRow.coordinateY =
								lastGeodetic.coordinateY?.plus(deltaY)?.roundTo5DecimalPlaces()
							addLastCoordiantesRow.measurementsId = lastGeodetic.measurementsId
							viewAdapter.data.add(addLastCoordiantesRow)
						} else {
							lastRow.coordinateX = measurements?.firstPointCoordinateX
							lastRow.coordinateY = measurements?.firstPointCoordinateY
							viewAdapter.data.add(lastRow)
						}
						viewAdapter.notifyDataSetChanged()
					}
				}
			}
		}
	}

	override fun onDeleteClick(measurement: Measurement) {
		val builder = AlertDialog.Builder(requireContext())

		with(builder)
		{
			setTitle("Предупреждение")
			setMessage("Вы точно хотите вернутся к этому измерению ?")
			setPositiveButton("Да") { dialog, _ ->
				deleteNextRows(measurement)
				dialog.dismiss()
			}
			setNegativeButton("Нет") { dialog, _ ->
				dialog.dismiss()
			}

			show()
		}
	}

	private fun deleteNextRows(measurement: Measurement) {
		val journalRow = measurement as? GeodeticRow ?: return
		val rowNumber = journalRow.rowNumber ?: -1
		val value = viewAdapter.data as MutableList<GeodeticRow>
		if (rowNumber != -1) {
			val rowsToDelete = value.filter {
				it.rowNumber?.let { row ->
					row > rowNumber
				} ?: true
			}
			val currentRowList = value.minus(rowsToDelete)
			if (currentRowList.isNotEmpty()) {
				previousDirectionalAngle = if (currentRowList.lastIndex > 0) {
					currentRowList[currentRowList.lastIndex - 1].directionalAngle
				} else null
				viewAdapter.data = currentRowList.toMutableList()
				viewAdapter.notifyDataSetChanged()
				GlobalScope.launch {
					context?.let {
						AppDatabase.getAppDataBase(it)
							?.journalRowDao()?.deleteGeodeticRows(rowsToDelete)
					}

				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {
		val journalRow = measurement as? GeodeticRow ?: return

		GlobalScope.launch {
			val db = AppDatabase.getAppDataBase(context = requireContext())
			val journalRowDao = db?.journalRowDao()
			journalRowDao?.insertJournalRow(journalRow.apply {
				measurementsId = measurements?.id ?: -1
			})
			lastRow = GeodeticRow()
			previousDirectionalAngle = journalRow.directionalAngle
			val radianDirection = Math.toRadians(journalRow.directionalAngle ?: 0.0)
			val rowHorizontalDistance = journalRow.horizontalDistance ?: 0.0
			val deltaX =
				rowHorizontalDistance.times(cos(radianDirection))
			lastRow.coordinateX = journalRow.coordinateX?.plus(deltaX)?.roundTo5DecimalPlaces()
			val deltaY = rowHorizontalDistance.times(sin(radianDirection))
			lastRow.coordinateY = journalRow.coordinateY?.plus(deltaY)?.roundTo5DecimalPlaces()
			lastRow.measurementsId = journalRow.measurementsId
			withContext(Dispatchers.Main) {
				journalRow.rowNumber?.let { rowNumber ->
					viewAdapter.data.set(
						rowNumber,
						journalRow
					)
				}
				viewAdapter.data.add(lastRow)
				viewAdapter.notifyDataSetChanged()
				journalLayout?.scrollToPosition(viewAdapter.itemCount - 1)
			}
		}
	}

	override fun calculateRow(itemView: View, position: Int) {
		val azimuth = itemView.azimuth.text.toString().toDoubleOrNull()
		val rhumb = MathHelper.fromAzimuthToRhumb(azimuth?.roundTo1DecimalPlaces())
		val directionalAngle = itemView.directional_angle.text.toString().toDoubleOrNull()
		val insideAngle = measurements?.travelDirection?.let {
			MathHelper.fromDirectionalToInside(
				it,
				directionalAngle,
				previousDirectionalAngle
			)
		}
		if (azimuth != null) {
			itemView.directional_angle.setText(
				azimuth.plus(
					measurements?.magneticDeclination ?: 0.0
				).toString()
			)
		}
		itemView.rhumb.setText(rhumb)
		itemView.insideCorner.setText(insideAngle.toString())
	}
}