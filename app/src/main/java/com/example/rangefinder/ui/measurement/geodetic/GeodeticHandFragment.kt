package com.example.rangefinder.ui.measurement.geodetic

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.rangefinder.R
import com.example.rangefinder.data.helper.MathHelper
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.data.model.GeodeticHandRow
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.roundTo1DecimalPlaces
import com.example.rangefinder.roundTo5DecimalPlaces
import com.example.rangefinder.ui.measurement.AngleTypes
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.MeasurementAdapter
import com.example.rangefinder.ui.measurement.MeasurementCallback
import kotlinx.android.synthetic.main.fragment_hand_geodetic.*
import kotlinx.android.synthetic.main.geodetic_hand_row_item.view.*
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import kotlin.math.cos
import kotlin.math.sin

class GeodeticHandFragment : Fragment(), MeasurementCallback {

	private lateinit var viewAdapter: MeasurementAdapter
	private var lastRow = GeodeticHandRow()
	private var previousDirectionalAngle: Double? = null

	private var measurements: CommonMeasurements? = null

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"

		fun newInstance(commonMeasurements: CommonMeasurements): GeodeticHandFragment =
			GeodeticHandFragment().apply {
				arguments = Bundle().apply {
					putSerializable(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
				}
			}
	}

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_hand_geodetic, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)

		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements

		viewAdapter = MeasurementAdapter(this)
		recyclerViewHandGeodetic.apply {
			layoutManager = LinearLayoutManager(context)
			adapter = this@GeodeticHandFragment.viewAdapter
		}
		initPreviousMeasurements()
	}

	private fun initPreviousMeasurements() {
		val measurementID = (activity as? BaseMeasurementActivity)?.commonMeasurements?.id
		GlobalScope.launch {
			context?.let {
				if (measurementID != null) {
					val measurement = AppDatabase.getAppDataBase(it)
						?.geodeticHandRowDao()?.getMeasurementById(measurementID)

					withContext(Dispatchers.Main) {
						if (measurement != null && measurement.isNotEmpty()) {
							viewAdapter.data.addAll(measurement)
							val addLastCoordiantesRow = GeodeticHandRow()
							val lastGeodetic = measurement.last()
							previousDirectionalAngle = lastGeodetic.directionalAngle
							val radianDirection = lastGeodetic.directionalAngle ?: 0.0
							val deltaX =
								lastGeodetic.horizontalDistance?.times(
									cos(
										Math.toRadians(
											radianDirection
										)
									)
								) ?: 0.0
							addLastCoordiantesRow.coordinateX =
								lastGeodetic.coordinateX?.plus(deltaX)?.roundTo5DecimalPlaces()
							addLastCoordiantesRow.rowNumber = lastGeodetic.rowNumber?.plus(1)
							val deltaY = lastGeodetic.horizontalDistance?.times(
								sin(
									Math.toRadians(radianDirection)
								)
							) ?: 0.0
							addLastCoordiantesRow.coordinateY =
								lastGeodetic.coordinateY?.plus(deltaY)?.roundTo5DecimalPlaces()
							addLastCoordiantesRow.measurementsId = lastGeodetic.measurementsId
							viewAdapter.data.add(addLastCoordiantesRow)
						} else {
							lastRow.coordinateX = measurements?.firstPointCoordinateX
							lastRow.coordinateY = measurements?.firstPointCoordinateY
							viewAdapter.data.add(lastRow)
						}

						viewAdapter.notifyDataSetChanged()
					}
				}
			}
		}
	}

	override fun onDeleteClick(measurement: Measurement) {
		val builder = AlertDialog.Builder(requireContext())

		with(builder)
		{
			setTitle("Предупреждение")
			setMessage("Вы точно хотите вернутся к этому измерению ?")
			setPositiveButton("Да") { dialog, _ ->
				deleteNextRows(measurement)
				dialog.dismiss()
			}
			setNegativeButton("Нет") { dialog, _ ->
				dialog.dismiss()
			}

			show()
		}

	}

	private fun deleteNextRows(measurement: Measurement) {
		val journalRow = measurement as? GeodeticHandRow ?: return
		val rowNumber = journalRow.rowNumber ?: -1
		val value = viewAdapter.data as MutableList<GeodeticHandRow>
		if (rowNumber != -1) {
			val rowsToDelete = value.filter {
				it.rowNumber?.let { row ->
					row > rowNumber
				} ?: true
			}
			val currentRowList = value.minus(rowsToDelete)
			if (currentRowList.isNotEmpty()) {
				previousDirectionalAngle = if (currentRowList.lastIndex > 0) {
					currentRowList[currentRowList.lastIndex - 1].directionalAngle
				} else null
				viewAdapter.data = currentRowList.toMutableList()
				viewAdapter.notifyDataSetChanged()
				GlobalScope.launch {
					context?.let {
						AppDatabase.getAppDataBase(it)
							?.geodeticHandRowDao()?.deleteGeodeticHandRows(rowsToDelete)
					}

				}
			}
		}
	}

	override fun onSaveClick(measurement: Measurement) {
		val journalRow = measurement as? GeodeticHandRow ?: return
		GlobalScope.launch {
			context?.let {
				val id = AppDatabase.getAppDataBase(it)
					?.geodeticHandRowDao()?.insertJournalRow(journalRow.apply {
						measurementsId = measurements?.id ?: -1
					})

				journalRow.id = id ?: -1
				previousDirectionalAngle = journalRow.directionalAngle
				lastRow = GeodeticHandRow()
				val radianDirection = Math.toRadians(journalRow.directionalAngle ?: 0.0)
				val rowHorizontalDistance = journalRow.horizontalDistance ?: 0.0
				val deltaX = rowHorizontalDistance.times(cos(radianDirection))
				lastRow.coordinateX = journalRow.coordinateX?.plus(deltaX)?.roundTo5DecimalPlaces()
				val deltaY = rowHorizontalDistance.times(sin(radianDirection))
				lastRow.coordinateY = journalRow.coordinateY?.plus(deltaY)?.roundTo5DecimalPlaces()
				lastRow.measurementsId = journalRow.measurementsId
				withContext(Dispatchers.Main) {
					journalRow.rowNumber?.let { rowNumber ->
						viewAdapter.data.set(
							rowNumber,
							journalRow
						)
					}
					viewAdapter.data.add(lastRow)
					viewAdapter.notifyDataSetChanged()
					recyclerViewHandGeodetic?.scrollToPosition(viewAdapter.itemCount - 1)
				}
			}

		}
	}

	private fun disableFields(views: List<View>) {
		views.forEach {
			it.isEnabled = false
		}
	}

	override fun disableColumns(itemView: View, position: Int) {
		measurements = arguments?.getSerializable(EXTRA_GEODETIC_MEASUREMENTS)
				as? CommonMeasurements
		itemView.horizontal_distance.isEnabled = false
		itemView.directional_angle.isEnabled = false
		if (position == 0) {
			when (measurements?.handModeFirstAngle) {
				AngleTypes.RHUMB.label -> disableFields(
					listOf(
						itemView.azimuth,
						itemView.insideCorner
					)
				)
				AngleTypes.AZIMUTH.label -> disableFields(
					listOf(
						itemView.rhumb,
						itemView.insideCorner
					)
				)
			}
		} else {
			when (measurements?.handModeAngle) {
				AngleTypes.AZIMUTH.label -> disableFields(
					listOf(
						itemView.rhumb,
						itemView.insideCorner
					)
				)
				AngleTypes.RHUMB.label -> disableFields(
					listOf(
						itemView.azimuth,
						itemView.insideCorner
					)
				)
				AngleTypes.INSIDE.label -> disableFields(
					listOf(
						itemView.azimuth,
						itemView.rhumb
					)
				)
			}
		}
	}

	override fun calculateRow(itemView: View, position: Int) {
		val verticalAngle = itemView.verticalAngle.text.toString().toDoubleOrNull() ?: 0.0
		val slantDistance = itemView.slantDistance.text.toString().toDoubleOrNull()
		val radianAngle = Math.toRadians(verticalAngle)
		val distance = slantDistance?.times(cos(radianAngle))?.roundTo1DecimalPlaces()
		itemView.horizontal_distance.setText(distance.toString())
		if (position == 0) {
			when (measurements?.handModeFirstAngle) {
				AngleTypes.RHUMB.label -> calculateRowByRhumb(itemView)
				AngleTypes.AZIMUTH.label -> calculateRowByAzimuth(itemView)
			}
		} else {
			when (measurements?.handModeAngle) {
				AngleTypes.RHUMB.label -> calculateRowByRhumb(itemView)
				AngleTypes.AZIMUTH.label -> calculateRowByAzimuth(itemView)
				AngleTypes.INSIDE.label -> calculateRowByInside(itemView)
			}
		}
	}

	private fun calculateRowByRhumb(itemView: View) {
		val rhumb = itemView.rhumb.text.toString()
		val azimuth = MathHelper.fromRhumbToAzimuth(rhumb)
		val directionalAngle = azimuth?.let { measurements?.magneticDeclination?.plus(it) }
		val insideAngle = measurements?.travelDirection?.let {
			MathHelper.fromDirectionalToInside(
				it,
				directionalAngle,
				previousDirectionalAngle
			)
		}
		itemView.azimuth.setText(azimuth.toString())
		itemView.directional_angle.setText(directionalAngle.toString())
		itemView.insideCorner.setText(insideAngle.toString())
	}

	private fun calculateRowByAzimuth(itemView: View) {
		val azimuth = itemView.azimuth.text.toString().toDoubleOrNull()
		val rhumb = MathHelper.fromAzimuthToRhumb(azimuth?.roundTo1DecimalPlaces())
		val directionalAngle = azimuth?.let { measurements?.magneticDeclination?.plus(it) }
		val insideAngle = measurements?.travelDirection?.let {
			MathHelper.fromDirectionalToInside(
				it,
				directionalAngle,
				previousDirectionalAngle
			)
		}
		itemView.rhumb.setText(rhumb)
		itemView.directional_angle.setText(directionalAngle.toString())
		itemView.insideCorner.setText(insideAngle.toString())

	}

	private fun calculateRowByInside(itemView: View) {
		val insideAngle = itemView.insideCorner.text.toString().toDoubleOrNull()
		val directionAngle = measurements?.travelDirection?.let {
			MathHelper.getDirectionalFromInside(
				previousDirectionalAngle,
				insideAngle,
				it
			)
		}
		val azimuth = measurements?.magneticDeclination?.let { directionAngle?.minus(it) }
		val rhumb = MathHelper.fromAzimuthToRhumb(azimuth?.roundTo1DecimalPlaces())
		itemView.azimuth.setText(azimuth.toString())
		itemView.rhumb.setText(rhumb)
		itemView.directional_angle.setText(directionAngle.toString())
	}
}