package com.example.rangefinder.ui

import com.example.rangefinder.R
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.circle.CircleActivity
import com.example.rangefinder.ui.measurement.geodetic.GeodeticActivity
import com.example.rangefinder.ui.measurement.height.HeightActivity
import com.example.rangefinder.ui.measurement.squre.SquareActivity
import com.example.rangefinder.ui.measurement.treecount.TreeCountActivity

enum class MeasurementType(
	val typeNameResourceId: Int,
	val relatedActivityClass: Class<out BaseMeasurementActivity>,
	val position: Int
) {
	

	VALUE_OF_GEODETIC(
		R.string.measurement_2,
		GeodeticActivity::class.java,
		0
	),
	VALUE_OF_DIAMETER(
		R.string.measurement_1,
		TreeCountActivity::class.java,
		1
	),
	CIRCLE(
		R.string.measurement_3,
		CircleActivity::class.java,
		2
	),
	SQUARE(
		R.string.measurement_4,
		SquareActivity::class.java,
		3
	),
	HEIGHT(
		R.string.measurement_5,
		HeightActivity::class.java,
		4
	)
}