package com.example.rangefinder.ui.measurement.geodetic.setup

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.roundTo5DecimalPlaces
import com.example.rangefinder.services.location.Location
import com.example.rangefinder.services.location.locationListener
import com.example.rangefinder.ui.measurement.AngleTypes
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import com.example.rangefinder.ui.measurement.TravelDirection
import com.google.android.gms.location.LocationResult
import kotlinx.android.synthetic.main.fragment_geodetic_setup.*
import kotlinx.android.synthetic.main.fragment_geodetic_setup.view.*
import org.osgeo.proj4j.BasicCoordinateTransform
import org.osgeo.proj4j.CRSFactory
import org.osgeo.proj4j.ProjCoordinate


class GeodeticSetupFragment : Fragment() {

	var location: Location? = null
	private var measurements: CommonMeasurements? = null

	override fun onCreateView(
		inflater: LayoutInflater,
		container: ViewGroup?,
		savedInstanceState: Bundle?
	): View? {
		return inflater.inflate(R.layout.fragment_geodetic_setup, container, false)
	}

	override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
		super.onViewCreated(view, savedInstanceState)
		val travelDirections = TravelDirection.values().map { it.label }
		val angles = AngleTypes.values().map { it.label }
		val firstAngleAdapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, listOf(
				AngleTypes.AZIMUTH.label,
				AngleTypes.RHUMB.label
			)
		)
		view.firstAngleSpinner.adapter = firstAngleAdapter
		val angleAdapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, angles
		)
		view.angleSpinner.adapter = angleAdapter
		val travelAdapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, travelDirections
		)
		view.travelDirectionSpinner.adapter = travelAdapter
		measurements =
			arguments?.getSerializable(BaseMeasurementSetupActivity.EXTRA_EXISTING_MEASUREMENTS)
					as? CommonMeasurements
		if (measurements != null) {
			view.firstAngleSpinner.setSelection(
				firstAngleAdapter.getPosition(
					measurements!!
						.handModeFirstAngle
				)
			)
			view.firstAngleSpinner.isEnabled = false
			view.angleSpinner.setSelection(
				angleAdapter.getPosition(
					measurements!!
						.handModeFirstAngle
				)
			)
			view.angleSpinner.isEnabled = false
			view.travelDirectionSpinner.setSelection(
				travelAdapter.getPosition(
					measurements!!
						.handModeFirstAngle
				)
			)
			view.travelDirectionSpinner.isEnabled = false
			coordinateXEdit.setText(
				measurements?.firstPointCoordinateX.toString()
			)
			coordinateXEdit.isEnabled =false
			coordinateYEdit.setText(
				measurements?.firstPointCoordinateY.toString()
			)
			coordinateYEdit.isEnabled =false
			locationButton.isEnabled =false
			declensionEdit.setText(measurements?.magneticDeclination.toString())
			declensionEdit.isEnabled = false
		} else {
			locationButton.setOnClickListener {
				location = Location(activity as AppCompatActivity, object : locationListener {
					override fun locationResponse(locationResult: LocationResult) {
						if (coordinateXEdit != null && coordinateYEdit != null) {
							val utmCoordinates =getUTMCoordinates(
								locationResult.lastLocation.latitude,
								locationResult.lastLocation.longitude
							)
							coordinateXEdit.setText(
								utmCoordinates.x.roundTo5DecimalPlaces().toString()
							)
							coordinateYEdit.setText(
								utmCoordinates.y.roundTo5DecimalPlaces().toString()
							)
						}
					}
				})
				location?.inicializeLocation()
			}
		}
	}

	private fun getUTMCoordinates(latitude: Double?, longitude: Double?): ProjCoordinate {
		val factory = CRSFactory()
		val srcCrs = factory.createFromName("EPSG:4326")
		val dstCrs = factory.createFromName("EPSG:2080")

		val transform = BasicCoordinateTransform(srcCrs, dstCrs)

		val srcCoord = ProjCoordinate(longitude ?: 0.0, latitude ?: 0.0)
		val dstCoord = ProjCoordinate()
		transform.transform(srcCoord, dstCoord)
		return dstCoord
	}

	override fun onDestroyView() {
		super.onDestroyView()
		location?.stopUpdateLocation()
	}
}