package com.example.rangefinder.ui.measurement

enum class TravelDirection(val label: String) {
	RIGHT("По часовой стрелке"),
	LEFT("Против часовой стрелки")
}