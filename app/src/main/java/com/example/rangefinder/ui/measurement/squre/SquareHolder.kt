package com.example.rangefinder.ui.measurement.squre

import android.graphics.Color
import android.view.View
import android.widget.ArrayAdapter
import com.example.rangefinder.data.model.JournalSquareRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.*
import kotlinx.android.synthetic.main.item_square.view.*

class SquareHolder(
	private val view: View,
	callback: MeasurementCallback? = null
) : MeasurementViewHolder(view, callback) {

	override fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		super.bind(measurement, adapter)
		val measurement = measurement as? JournalSquareRow ?: return
		val initialBackground = view.tree_Number.background
		view.row_number.text = adapterPosition.toString()
		measurement.treeNumber?.let { view.tree_Number.setText(it.toString()) }
			?: view.tree_Number.setText("")
		measurement.squareNumber?.let { view.square_Number.setText(it.toString()) }
			?: view.square_Number.setText("")
		measurement.breed?.let { view.breed.setText(it) } ?: view.breed.setText("")
		measurement.age?.let { view.age.setText(it.toString()) } ?: view.age.setText("")
		measurement.diameterOne?.let { view.diameter_One.setText(it.toString()) }
			?: view.diameter_One.setText("")
		measurement.diameterTwo?.let { view.diameter_Two.setText(it.toString()) }
			?: view.diameter_Two.setText("")
		measurement.height?.let { view.height_m.setText(it.toString()) }
			?: view.height_m.setText("")
		measurement.heightTwo?.let { view.heightTwo.setText(it.toString()) }
			?: view.heightTwo.setText("")
		measurement.heightThree?.let { view.heightThree.setText(it.toString()) }
			?: view.heightThree.setText("")
		measurement.diameterCrownOne?.let { view.diameterCrownOne.setText(it.toString()) }
			?: view.diameterCrownOne.setText("")
		measurement.diameterCrownTwo?.let { view.diameterCrownTwo.setText(it.toString()) }
			?: view.diameterCrownTwo.setText("")
		measurement.coordinateX?.let { view.coordinateX.setText(it.toString()) }
			?: view.coordinateX.setText("")
		measurement.coordinateY?.let { view.coordinateY.setText(it.toString()) }
			?: view.coordinateY.setText("")
		measurement.distanceCrown?.let { view.distanceCrown.setText(it.toString()) }
			?: view.distanceCrown.setText("")
		view.height_m.setBackgroundColor(Color.parseColor("#33FF0000"))
		view.height_m.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.heightTwo.background = initialBackground
			view.heightThree.background = initialBackground
			callback?.onFieldSelect("H1")
			return@setOnLongClickListener true
		}
		view.heightTwo.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.height_m.background = initialBackground
			view.heightThree.background = initialBackground
			callback?.onFieldSelect("H2")
			return@setOnLongClickListener true
		}
		view.heightThree.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.height_m.background = initialBackground
			view.heightTwo.background = initialBackground
			callback?.onFieldSelect("H3")
			return@setOnLongClickListener true
		}
		view.diameter_One.setBackgroundColor(Color.parseColor("#33ebe834"))
		view.diameter_One.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33ebe834"))
			view.diameter_Two.background = initialBackground
			callback?.onFieldSelect("D1")
			return@setOnLongClickListener true
		}
		view.diameter_Two.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33ebe834"))
			view.diameter_One.background = initialBackground
			callback?.onFieldSelect("D2")
			return@setOnLongClickListener true
		}

		initCategorySpinner(measurement)
		initTierSpinner(measurement)
		initGenerationSpinner(measurement)
		initCrownFormSpinner(measurement)
		initCrownTwoFormSpinner(measurement)
		initAutoCompleteBreed()
	}

	override fun fillMeasurement(measurement: Measurement) {
		val measurement = measurement as? JournalSquareRow ?: return

		measurement.rowNumber = view.row_number.text.toString().toInt()
		measurement.breed = view.breed.text.toString()
		measurement.squareNumber = view.square_Number.text.toString().toIntOrNull()
		measurement.treeNumber = view.tree_Number.text.toString().toIntOrNull()
		measurement.tier = view.tierSpinner.selectedItem as? String
		measurement.generation = view.generationSpinner.selectedItem as? String
		measurement.age = view.age.text.toString().toIntOrNull()
		measurement.height = view.height_m.text.toString().toDoubleOrNull()
		measurement.heightTwo = view.heightTwo.text.toString().toDoubleOrNull()
		measurement.heightThree = view.heightThree.text.toString().toDoubleOrNull()
		measurement.diameterOne = view.diameter_One.text.toString().toDoubleOrNull()
		measurement.diameterTwo = view.diameter_Two.text.toString().toDoubleOrNull()
		measurement.diameterCrownOne = view.diameterCrownOne.text.toString().toDoubleOrNull()
		measurement.diameterCrownTwo = view.diameterCrownTwo.text.toString().toDoubleOrNull()
		measurement.coordinateX = view.coordinateX.text.toString().toDoubleOrNull()
		measurement.coordinateY = view.coordinateY.text.toString().toDoubleOrNull()
		measurement.distanceCrown = view.distanceCrown.text.toString().toDoubleOrNull()

		measurement.category = view.categorySpinner.selectedItem as? String
		measurement.crownFormVertical = view.crownFormSpinner.selectedItem as? String
		measurement.crownFormHorizontal = view.crownFormTwoSpinner.selectedItem as? String
	}

	private fun initCategorySpinner(measurement: JournalSquareRow) {
		val categoriesNames = TreeCategory.values().map { it.displayName }
		view.categorySpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, categoriesNames
		)
		measurement.category?.let {
			var selectedCategoryPosition = 0
			categoriesNames.forEachIndexed { index, s ->
				if (s == it) selectedCategoryPosition = index
			}
			view.categorySpinner.setSelection(selectedCategoryPosition)
		}
	}
	private fun initTierSpinner(measurement: JournalSquareRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.tierSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.tier?.let {
			var selectedTierPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedTierPosition = index
			}
			view.tierSpinner.setSelection(selectedTierPosition)
		}
	}
	private fun initGenerationSpinner(measurement: JournalSquareRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.generationSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.generation?.let {
			var selectedGenerationPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedGenerationPosition = index
			}
			view.generationSpinner.setSelection(selectedGenerationPosition)
		}
	}
	private fun initCrownFormSpinner(measurement: JournalSquareRow) {
		val crownFormNames = CrownForm.values().map { it.displayName }
		view.crownFormSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, crownFormNames
		)
		measurement.crownFormVertical?.let {
			var selectedCrownFormPosition = 0
			crownFormNames.forEachIndexed { index, s ->
				if (s == it) selectedCrownFormPosition = index
			}
			view.crownFormSpinner.setSelection(selectedCrownFormPosition)
		}
	}
	private fun initCrownTwoFormSpinner(measurement: JournalSquareRow) {
		val crownFormNames = CrownFormHorizonal.values().map { it.displayName }
		view.crownFormTwoSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, crownFormNames
		)
		measurement.crownFormHorizontal?.let {
			var selectedCrownFormPosition = 0
			crownFormNames.forEachIndexed { index, s ->
				if (s == it) selectedCrownFormPosition = index
			}
			view.crownFormTwoSpinner.setSelection(selectedCrownFormPosition)
		}
	}
	private fun initAutoCompleteBreed() {
		val categoriesNames = TreeCodes.values().map { it.displayName }
		val adapter1 = ArrayAdapter(
			view.context, // Context
			android.R.layout.simple_dropdown_item_1line, // Layout
			categoriesNames // Array
		)
		view.breed.setAdapter(adapter1)
		view.breed.threshold = 1
		view.breed.onFocusChangeListener = View.OnFocusChangeListener { v, hasFocus ->
			if (hasFocus) {
				view.breed.showDropDown()
			}
		}
	}
}