package com.example.rangefinder.ui.measurement

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.ui.main.isLight

abstract class BaseMeasurementActivity : AppCompatActivity() {

	var commonMeasurements: CommonMeasurements? = null

	var inputMethod: InputMethod? = null

	companion object {

		private const val EXTRA_GEODETIC_MEASUREMENTS = "EXTRA_GEODETIC_MEASUREMENTS"
		private const val EXTRA_INPUT_METHOD = "EXTRA_INPUT_METHOD"

		fun getLaunchIntent(
			context: Context,
			commonMeasurements: CommonMeasurements,
			currentClass: Class<out BaseMeasurementActivity>,
			inputMethod: InputMethod
		): Intent = Intent(context, currentClass).apply {
			putExtra(EXTRA_GEODETIC_MEASUREMENTS, commonMeasurements)
			putExtra(EXTRA_INPUT_METHOD, inputMethod)
		}
	}

	override fun onCreate(savedInstanceState: Bundle?) {
		if (isLight)setTheme(R.style.AppThemeLight)
		else setTheme(R.style.AppTheme)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_geodetic)

		commonMeasurements =
			intent?.getSerializableExtra(EXTRA_GEODETIC_MEASUREMENTS) as? CommonMeasurements
		inputMethod =
			intent?.getSerializableExtra(EXTRA_INPUT_METHOD) as? InputMethod

		commonMeasurements ?: throw IllegalArgumentException(
			"Geodetic Measurements shouldn't be null"
		)
	}
}