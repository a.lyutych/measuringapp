package com.example.rangefinder.ui.measurement.treecount

import android.view.View
import android.widget.ArrayAdapter
import com.example.rangefinder.data.model.JournalTreeCount
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.*
import kotlinx.android.synthetic.main.item_diameter.view.*


class TreeCountHolder(
	private val view: View,
	callback: MeasurementCallback? = null
) : MeasurementViewHolder(view, callback) {

	override val disableSpinnersForSavedMeasurements = false

	override fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		super.bind(measurement, adapter)
		val measurement = measurement as? JournalTreeCount ?: return

		view.row_number.text = adapterPosition.toString()
		measurement.breed?.let { view.breed.setText(it) } ?: view.breed.setText("")
		measurement.diameter?.let { view.diameter.setText(it.toString()) } ?: view.diameter.setText("")
		initCategorySpinner(measurement)
		initTierSpinner(measurement)
		initGenerationSpinner(measurement)
		initAutoCompleteBreed()
		val isNotLast = adapterPosition != adapter.data.lastIndex
		itemView.editButton.visibility = if (isNotLast) View.VISIBLE else View.GONE
	}

	override fun fillMeasurement(measurement: Measurement) {
		val measurement = measurement as? JournalTreeCount ?: return
		measurement.rowNumber = view.row_number.text.toString().toInt()
		measurement.breed = view.breed.text.toString()
		measurement.diameter = view.diameter.text.toString().toDoubleOrNull()
		measurement.tier = view.tierSpinner.selectedItem as? String
		measurement.generation = view.generationSpinner.selectedItem as? String
		measurement.category = view.categorySpinner.selectedItem as? String
}

	private fun initCategorySpinner(measurement: JournalTreeCount) {
		val categoriesNames = TreeCategory.values().map { it.displayName }
		view.categorySpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, categoriesNames)
		measurement.category?.let {
			var selectedCategoryPosition = 0
			categoriesNames.forEachIndexed { index, s ->
				if (s == it) selectedCategoryPosition = index
			}
			view.categorySpinner.setSelection(selectedCategoryPosition)
		}
	}
	private fun initTierSpinner(measurement: JournalTreeCount) {
		val tierNames = Tier.values().map { it.displayName }
		view.tierSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.tier?.let {
			var selectedTierPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedTierPosition = index
			}
			view.tierSpinner.setSelection(selectedTierPosition)
		}
	}
	private fun initGenerationSpinner(measurement: JournalTreeCount) {
		val tierNames = Tier.values().map { it.displayName }
		view.generationSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.generation?.let {
			var selectedGenerationPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedGenerationPosition = index
			}
			view.generationSpinner.setSelection(selectedGenerationPosition)
		}
	}
	private fun initAutoCompleteBreed() {
		val categoriesNames = TreeCodes.values().map { it.displayName }
		val adapter1 = ArrayAdapter(
			view.context, // Context
			android.R.layout.simple_dropdown_item_1line, // Layout
			categoriesNames // Array
		)
		view.breed.setAdapter(adapter1)
		view.breed.threshold = 1
        view.breed.onFocusChangeListener= View.OnFocusChangeListener { v, hasFocus -> if(hasFocus){
			view.breed.showDropDown()
		} }
	}

}

