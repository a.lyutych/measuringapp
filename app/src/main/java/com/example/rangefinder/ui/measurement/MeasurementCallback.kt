package com.example.rangefinder.ui.measurement

import android.view.View
import com.example.rangefinder.data.model.Measurement

interface MeasurementCallback {

	fun onEditClick(measurement: Measurement) {}

	fun onDeleteClick(measurement: Measurement)

	fun onSaveClick(measurement: Measurement)

	fun disableColumns(itemView: View, position: Int) {}

	fun calculateRow(itemView: View, position: Int) {}

	fun onFieldSelect(field: String) {}
}