package com.example.rangefinder.ui.measurement

import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import com.example.rangefinder.R
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.disableAllInnerCheckBoxes
import com.example.rangefinder.disableAllInnerEditTexts
import com.example.rangefinder.disableAllInnerSpinners

abstract class MeasurementViewHolder(
	itemView: View,
	protected val callback: MeasurementCallback? = null
) : RecyclerView.ViewHolder(itemView) {

	protected open val disableSpinnersForSavedMeasurements = true

	open fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		val isLast = adapterPosition == (adapter.itemCount - 1)
		showSaveButtonOnlyForLastItem(this, isLast)
		disableAllFieldsBesidesLastOne(this, isLast)
		itemView.findViewById<View>(R.id.editButton)?.visibility = View.GONE

		callback?.disableColumns(itemView, adapterPosition)
		itemView.findViewById<View>(R.id.editButton)?.setOnClickListener {
			if (isValidInput()) {
				fillMeasurement(measurement)
				callback?.onEditClick(measurement)
			} else {
				onInvalidInput()
			}
		}
		itemView.findViewById<View>(R.id.deleteButton)?.setOnClickListener {
			fillMeasurement(measurement)
			callback?.onDeleteClick(measurement)
		}
		itemView.findViewById<View>(R.id.saveButton)?.setOnClickListener {
			callback?.calculateRow(itemView, adapterPosition)
			if (isValidInput()) {
				fillMeasurement(measurement)
				callback?.onSaveClick(measurement)
			} else {
				onInvalidInput()
			}
		}
	}

	private fun onInvalidInput() {
		Toast.makeText(
			itemView.context,
			itemView.context.getString(R.string.invalid_arguments),
			Toast.LENGTH_SHORT
		).show()
	}

	open fun isValidInput(): Boolean = true

	abstract fun fillMeasurement(measurement: Measurement)

	private fun disableAllFieldsBesidesLastOne(holder: MeasurementViewHolder, last: Boolean) {
		(holder.itemView as? ViewGroup)?.disableAllInnerEditTexts(last)
		(holder.itemView as? ViewGroup)?.disableAllInnerCheckBoxes(last)
		if (disableSpinnersForSavedMeasurements) {
			(holder.itemView as? ViewGroup)?.disableAllInnerSpinners(last)
		}
	}

	private fun showSaveButtonOnlyForLastItem(holder: MeasurementViewHolder, isLast: Boolean) {
		holder.itemView.findViewById<View>(R.id.saveButton)?.visibility =
			if (isLast) View.VISIBLE else View.GONE
	}
}