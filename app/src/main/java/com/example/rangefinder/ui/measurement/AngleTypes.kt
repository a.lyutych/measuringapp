package com.example.rangefinder.ui.measurement

enum class AngleTypes(val label: String) {
	AZIMUTH("Азимут"),
	RHUMB("Румб"),
	INSIDE("Внутренний угол")
}