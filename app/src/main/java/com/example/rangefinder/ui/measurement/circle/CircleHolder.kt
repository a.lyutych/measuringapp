package com.example.rangefinder.ui.measurement.circle

import android.graphics.Color
import android.view.View
import android.widget.ArrayAdapter
import com.example.rangefinder.data.model.JournalRingCharacteristicsRow
import com.example.rangefinder.data.model.Measurement
import com.example.rangefinder.ui.measurement.*
import kotlinx.android.synthetic.main.item_ring_characteristics.view.*

class CircleHolder(
	private val view: View,
	callback: MeasurementCallback? = null
) : MeasurementViewHolder(view, callback) {

	override fun bind(measurement: Measurement, adapter: MeasurementAdapter) {
		super.bind(measurement, adapter)
		val measurement = measurement as? JournalRingCharacteristicsRow ?: return
		val initialBackground = view.breed.background
		view.row_number.text = adapterPosition.toString()
		measurement.age?.let { view.age.setText(it.toString()) } ?: view.age.setText("")
		measurement.breed?.let { view.breed.setText(it) } ?: view.breed.setText("")
		measurement.diameter?.let { view.diameter.setText(it.toString()) } ?: view.diameter.setText(
			""
		)
		measurement.height?.let { view.height_m.setText(it.toString()) }
			?: view.height_m.setText("")
		measurement.direction?.let { view.direction.setText(it.toString()) }
			?: view.direction.setText("")
		measurement.horizontalDistance?.let { view.horizontalDistance.setText(it.toString()) }
			?: view.horizontalDistance.setText("")
		view.direction.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.horizontalDistance.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.height_m.background = initialBackground
			callback?.onFieldSelect("D")
			return@setOnLongClickListener true
		}
		view.horizontalDistance.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.direction.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.height_m.background = initialBackground
			callback?.onFieldSelect("D")
			return@setOnLongClickListener true
		}
		view.horizontalDistance.setBackgroundColor(Color.parseColor("#33FF0000"))
		view.direction.setBackgroundColor(Color.parseColor("#33FF0000"))
		view.height_m.setOnLongClickListener {
			it.setBackgroundColor(Color.parseColor("#33FF0000"))
			view.direction.background = initialBackground
			view.horizontalDistance.background = initialBackground
			callback?.onFieldSelect("H")
			return@setOnLongClickListener true
		}
		initCategorySpinner(measurement)
		initTierSpinner(measurement)
		initGenerationSpinner(measurement)
		initAutoCompleteBreed()
	}

	override fun fillMeasurement(measurement: Measurement) {
		val measurement = measurement as? JournalRingCharacteristicsRow ?: return

		measurement.rowNumber = view.row_number.text.toString().toInt()
		measurement.breed = view.breed.text.toString()
		measurement.tier = view.tierSpinner.selectedItem as? String
		measurement.generation = view.generationSpinner.selectedItem as? String
		measurement.age = view.age.text.toString().toIntOrNull()
		measurement.height = view.height_m.text.toString().toDoubleOrNull()
		measurement.diameter = view.diameter.text.toString().toDoubleOrNull()
		measurement.direction = view.direction.text.toString().toDoubleOrNull()
		measurement.horizontalDistance = view.horizontalDistance.text.toString().toDoubleOrNull()
		measurement.category = view.categorySpinner.selectedItem as? String
	}

	private fun initCategorySpinner(measurement: JournalRingCharacteristicsRow) {
		val categoriesNames = TreeCategory.values().map { it.displayName }
		view.categorySpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, categoriesNames
		)
		measurement.category?.let {
			var selectedCategoryPosition = 0
			categoriesNames.forEachIndexed { index, s ->
				if (s == it) selectedCategoryPosition = index
			}
			view.categorySpinner.setSelection(selectedCategoryPosition)
		}
	}
	private fun initTierSpinner(measurement: JournalRingCharacteristicsRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.tierSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.tier?.let {
			var selectedTierPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedTierPosition = index
			}
			view.tierSpinner.setSelection(selectedTierPosition)
		}
	}
	private fun initGenerationSpinner(measurement: JournalRingCharacteristicsRow) {
		val tierNames = Tier.values().map { it.displayName }
		view.generationSpinner.adapter = ArrayAdapter(
			view.context, android.R.layout.simple_spinner_item, tierNames
		)
		measurement.generation?.let {
			var selectedGenerationPosition = 0
			tierNames.forEachIndexed { index, s ->
				if (s == it) selectedGenerationPosition = index
			}
			view.generationSpinner.setSelection(selectedGenerationPosition)
		}
	}
	private fun initAutoCompleteBreed() {
		val categoriesNames = TreeCodes.values().map { it.displayName }
		val adapter1 = ArrayAdapter(
			view.context, // Context
			android.R.layout.simple_dropdown_item_1line, // Layout
			categoriesNames // Array
		)
		view.breed.setAdapter(adapter1)
		view.breed.threshold = 1
		view.breed.onFocusChangeListener= View.OnFocusChangeListener { v, hasFocus -> if(hasFocus){
			view.breed.showDropDown()
		} }
	}
}