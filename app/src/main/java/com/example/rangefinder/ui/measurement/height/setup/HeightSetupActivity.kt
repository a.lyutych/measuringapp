package com.example.rangefinder.ui.measurement.height.setup

import android.app.Activity
import android.content.Intent
import androidx.fragment.app.Fragment
import com.example.rangefinder.R
import com.example.rangefinder.data.model.CommonMeasurements
import com.example.rangefinder.getFragmentInContainer
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.measurement.BaseMeasurementActivity
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import com.example.rangefinder.ui.measurement.InputMethod
import com.example.rangefinder.ui.measurement.height.HeightActivity

class HeightSetupActivity : BaseMeasurementSetupActivity() {

	override val measurementType: MeasurementType = MeasurementType.HEIGHT

	override val showSecondSpinner: Boolean = true

	override fun saveGeodeticMeasurements(callback: (CommonMeasurements) -> Unit) {
		saveEmptyGeodeticMeasurements(callback)
	}
	override fun insertMeasurementsFields(measurement: CommonMeasurements): CommonMeasurements{
		measurement.apply {
			val setupFragment = getFragmentInContainer(R.id.fragmentContainer)
			initialHeight = setupFragment?.view
				?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id.initial_height)?.text.toString()
				.toDoubleOrNull()
		}
		return measurement
	}
	override fun isValidInput(): Boolean {
		val setupFragment = getFragmentInContainer(R.id.fragmentContainer)
		var isValid = true
		when {
			setupFragment?.view?.findViewById<androidx.appcompat.widget.AppCompatEditText>(R.id
				.initial_height)?.text.isNullOrBlank() -> {
				isValid = false
			}
		}
		return isValid
	}


	override fun launchMeasurementScreen(
		commonMeasurements: CommonMeasurements,
		inputMethod: InputMethod
	) =
		startActivityForResult(
			BaseMeasurementActivity.getLaunchIntent(
				this, commonMeasurements, HeightActivity::class.java, inputMethod
			),1
		)

	override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
		if (requestCode != BLUETOOTH_CODE) {
			setResult(Activity.RESULT_OK, data)
			finish()
			super.onActivityResult(requestCode, resultCode, data)
		} else {
			initDeviceSpinners()
		}
	}


	override fun getSetupContentFragment(): Fragment? = HeightSetupFragment()
}