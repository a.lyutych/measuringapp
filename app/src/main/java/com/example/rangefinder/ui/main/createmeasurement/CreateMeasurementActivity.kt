package com.example.rangefinder.ui.main.createmeasurement

import android.os.Bundle
import android.view.Menu
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.rangefinder.R
import com.example.rangefinder.data.local.database.AppDatabase
import com.example.rangefinder.ui.MeasurementType
import com.example.rangefinder.ui.main.isLight
import com.example.rangefinder.ui.measurement.BaseMeasurementSetupActivity
import kotlinx.android.synthetic.main.activity_create_measurement.*
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.launch

class CreateMeasurementActivity : AppCompatActivity() {

	private  var listOfNames : List<String> = emptyList()

	override fun onCreate(savedInstanceState: Bundle?) {
		if (isLight)setTheme(R.style.AppThemeLight)
		else setTheme(R.style.AppTheme)
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_create_measurement)
		GlobalScope.launch {
			val measurements =
				AppDatabase.getAppDataBase(this@CreateMeasurementActivity)
					?.commonMeasurementsDao()?.getMeasurements()
			if (measurements != null) {
				listOfNames = measurements.map { it.name }
			}
		}

		val modeNames = MeasurementType.values().map { getString(it.typeNameResourceId) }
		val modeAdapter =
			ArrayAdapter(this, android.R.layout.simple_list_item_1, modeNames)
		modeSpinner.adapter = modeAdapter

		nextButton?.setOnClickListener {
			val selectedItemPosition = modeSpinner.selectedItemPosition
			val selectedItem = MeasurementType.values().getOrNull(selectedItemPosition)
			nextFrame(selectedItem)
		}
	}

	private fun nextFrame(measurementType: MeasurementType?) {
		if (measurementType == null) return

		val geodeticMeasurementsName = measurementsName?.text?.toString() ?: ""
		if (geodeticMeasurementsName.isBlank() || listOfNames.contains(geodeticMeasurementsName)) {
			Toast.makeText(
				this,
				getString(R.string.invalid_arguments),
				Toast.LENGTH_SHORT
			).show()
		} else {
			val measurementsSetupIntent = BaseMeasurementSetupActivity.getLaunchIntent(
				this,
				measurementType,
				geodeticMeasurementsName
			)
			startActivity(measurementsSetupIntent)
			finish()
		}
	}
}
