package com.example.rangefinder

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import com.facebook.stetho.Stetho

class MyApplication : Application() {

	init {
		AppCompatDelegate.setDefaultNightMode(
			AppCompatDelegate.MODE_NIGHT_YES
		)
	}

	override fun onCreate() {
		super.onCreate()
		Stetho.initializeWithDefaults(this)
	}
}